-   Routine en cas d'écart entre les matrices de transformation             --> En cours
-   Commentaires et documentation                                           --> Bien avancé
-   Repère :
    -   Améliorer l'espace cible de la transformation de perspective        --> A faire
-   Cas si transformée récente trouvée mais pas de table trouvée ( image -> caca )