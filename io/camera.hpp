#ifndef __VISION__IO__CAMERA__
#define __VISION__IO__CAMERA__

#include <lccv.hpp>
#include "calibration.hpp"
#include "io.hpp"
#include "../plateau/detectionWork.hpp"



namespace Vision::io{
/**
 * TODO
*/
lccv::PiCamera* setupCamera(io::calibration Calibration);

/**
 * TODO
*/
lccv::PiCamera* resetCamera(lccv::PiCamera *cam);

/**
 * @brief Change l'exposition de la camera et redémarre le flux vidéo
 * @param camera caméra sur laquel opérer
 * @param expo exposition comprise entre +2.0 et -0.5
*/
void changeEv(lccv::PiCamera& camera, float expo);

/**
 * @brief Incrémente l'exposition de la camera et redémarre le flux vidéo
 * @param camera caméra sur laquel opérer
 * @param expo incrément positif ou négatif
*/
void incrementEv(lccv::PiCamera& camera, float expo);

/**
 * @brief Change le shutter de la camera et redémarre le flux vidéo
 * @param camera caméra sur laquel opérer
 * @param shutter valeure comprise entre 5000 et 1 µs
*/
void changeShutter(lccv::PiCamera& camera, float shutter);

/**
 * @brief Incrémente le shutter de la camera et redémarre le flux vidéo
 * @param camera caméra sur laquel opérer
 * @param shutter incrément positif ou négatif
*/
void incrementShutter(lccv::PiCamera& camera, float shutter);

/**
 * @brief Ajuste l'exposition et le shutter de la caméra pour éviter une image surexposée.
 * @param cam Caméra
 * @param raw Image brute
 * @param image Image dé-fisheye-isée
 * @param sortie Image resizée
 * @param cartes Correction de fisheye
 * @return Retourne 1 si échec, 0 sinon
*/
int setupCameraGain(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes);

/**
 * TODO
*/
int setupCameraGradiant(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes);

/**
* TODO
*/
int setupCameraTags(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes);

/**
 * TODO
*/
void ExpoTest(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes);

}

#endif