#include "camera.hpp"



namespace Vision::io{

    lccv::PiCamera* setupCamera(io::calibration Calibration){
        lccv::PiCamera *cam = new lccv::PiCamera();
    
        loadConfigCamera(Calibration, cam);
    
        return cam;
    }

    lccv::PiCamera* resetCamera(lccv::PiCamera *cam){
        cam->stopVideo();
        lccv::PiCamera *newCam = new lccv::PiCamera;

        newCam->options->video_height = cam->options->video_height;
        newCam->options->video_width = cam->options->video_width;
        newCam->options->framerate = cam->options->framerate;
        newCam->options->photo_height = cam->options->photo_height;
        newCam->options->photo_width = cam->options->photo_width;

        newCam->options->brightness = cam->options->brightness;
        newCam->options->contrast = cam->options->contrast;
        newCam->options->ev = cam->options->ev;
        newCam->options->gain = cam->options->gain;
        newCam->options->saturation = cam->options->saturation;
        newCam->options->sharpness = cam->options->sharpness;
        newCam->options->shutter = cam->options->shutter;

        delete cam;
        
        newCam->startVideo();
        return newCam; 
    }

    void changeEv(lccv::PiCamera& camera, float expo){
        camera.stopVideo();
        camera.options->ev = expo;
        camera.startVideo();
    }

    void incrementEv(lccv::PiCamera& camera, float increment){
        camera.stopVideo();
        camera.options->ev = camera.options->ev+increment;
        camera.startVideo();
    }

    void changeShutter(lccv::PiCamera& camera, float shutter){
        camera.stopVideo();
        camera.options->shutter = shutter;
        camera.startVideo();
    }

    void incrementShutter(lccv::PiCamera& camera, float shutter){
        camera.stopVideo();
        camera.options->shutter = camera.options->shutter+shutter;
        camera.startVideo();
    }

    int setupCameraGain(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes){
        const float minEv = -2.0;
        const float maxEv = 2.0;
        const float minShutter = 10000.0;
        const float maxShutter = 30000.0;
        float evStep = 0.2;
        float shutterStep = 10000.0;
        double burntThreshold = 1.0;
        double burnt;
        int failure = 0;
        int succes = 0;        
        cam.options->ev = maxEv+evStep;
        cam.options->shutter = maxShutter+shutterStep;        
        cam.startVideo();
        
        while (true) {
            if (!cam.getVideoFrame(raw, 100)) {
                fprintf(stderr, "ERR/Capture/Timeout\n");
                fflush(stderr);
            } 
            else {
                io::appliqueFisheye(raw, image, cartes);
                resize(image , sortie, Size(960,720));
                
                burnt = io::histogrammeValeurs(sortie);
                
                if(burnt>burntThreshold && cam.options->ev>minEv){
                    incrementEv(cam, -evStep);
                }
                else if (burnt>burntThreshold && cam.options->shutter>minShutter){
                    incrementShutter(cam, -shutterStep);
                }
                else if (burnt>burntThreshold){
                    printf("Limits reached !\n");
                    failure++;
                    if(failure>4) return 1;
                }
                else{
                    printf("Calibration OK !\n");
                    succes++;
                    if(succes>4) return 0;
                }         
                printf("%% : %f\n",burnt);
                printf("EV : %.1f\n",cam.options->ev);
                printf("Shutter : %f\n\n",cam.options->shutter);                  
            }            
        }
    }

    int setupCameraGradiant(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes){
        const float minEv = -2.0;           //Constantes car limites de la caméra
        const float maxEv = 2.0;
        const float minShutter = 1000.0;
        const float maxShutter = 30000.0;
        float evStep = 0.2;
        float shutterStep = 10000.0;
        int evIter = (int)((maxEv-minEv)/evStep);
        int shutterIter = (int)((maxShutter-minShutter)/shutterStep);
        vector<double> gradVect;
        vector<float> shutterVect;
        vector<float> evVect;     
        cam.startVideo();
        
        for(int i = 0; i<=evIter; i){
            if (!cam.getVideoFrame(raw, 100)) {
                fprintf(stderr, "ERR/Capture/Timeout\n");
                fflush(stderr);
            } 
            else {
                io::appliqueFisheye(raw, image, cartes);
                resize(image , sortie, Size(960,720));                
                changeEv(cam, maxEv-i*evStep);
                gradVect.push_back(io::gradientMagnitude(sortie));
                shutterVect.push_back(cam.options->shutter);
                evVect.push_back(cam.options->ev);                             
                printf("Grad. Mag. : %f\n",gradVect.back());
                printf("EV : %.1f\n",evVect.back());
                printf("Shutter : %f\n\n",shutterVect.back());
                i++;                  
            }            
        }
        printf("End of phase 1\n");
        for(int i = 0; i<=shutterIter; i){
            if (!cam.getVideoFrame(raw, 100)) {
                fprintf(stderr, "ERR/Capture/Timeout\n");
                fflush(stderr);
            } 
            else {
                io::appliqueFisheye(raw, image, cartes);
                resize(image , sortie, Size(960,720));
                printf("Trying to assign %f\n",maxShutter-i*shutterStep);
                changeShutter(cam, maxShutter-i*shutterStep);
                gradVect.push_back(io::gradientMagnitude(sortie));
                shutterVect.push_back(cam.options->shutter);
                evVect.push_back(cam.options->ev);      
                printf("Grad. Mag. : %f\n",gradVect.back());
                printf("EV : %.1f\n",evVect.back());
                printf("Shutter : %f\n\n",shutterVect.back());
                i++;                  
            }            
        }
        printf("End of phase 2\n");
        //Find index of max value of gradient
        int index = 0;
        for(int i = 0; i<gradVect.size(); i++){
            if(gradVect[i] > gradVect[index]){
                index = i;
            }
        }
        float newShutter = shutterVect[index];
        float newEv = evVect[index];
        printf("Shutter : %f\n", newShutter);
        printf("EV : %f\n", newEv);         
        cam.stopVideo();
        cam.options->ev = newEv;
        cam.options->shutter = newShutter;          
        printf("Fin de calibration :\n");
        printf("\tMax. Grad. : %f\n",gradVect[index]);
        printf("\tEV : %.1f\n",evVect[index]);
        printf("\tShutter : %f\n\n",shutterVect[index]);
        
        return 0;
    }

    int setupCameraTags(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes){
        const float minEv = -2.0;           //Constantes car limites de la caméra
        const float maxEv = 2.0;
        const float minShutter = 10000.0;
        const float maxShutter = 30000.0;
        float evStep = 0.2;
        float shutterStep = 10000.0;
        int evIter = (int)((maxEv-minEv)/evStep);
        int shutterIter = (int)((maxShutter-minShutter)/shutterStep);
        vector<plateau::arucoTag> tags;                 // Contient les tous les tags détectés
        vector<plateau::arucoTagCandidate> bad_tags;    // Contient les tag candidats rejetés     
        cam.startVideo();
        
        int i = 0;
        bool stop = false;
        do{
            if (!cam.getVideoFrame(raw, 200)) {
                fprintf(stderr, "ERR/Capture/Timeout\n");
                fflush(stderr);
            } 
            else {
                io::appliqueFisheye(raw, image, cartes);
                changeEv(cam, maxEv-i*evStep);
                cout << "Trying at ev " << maxEv-i*evStep << endl; 
                plateau::detectAruco(image, tags, bad_tags, plateau::coordSpace::untransformed, Point3f(1500, 2074, 1653), false);
                plateau::listAruco(tags, false);
                if(plateau::isTableAruco(tags)){
                    cout << "All tags found !" << endl;
                    stop = true;
                }
                else{
                    cout << "Not all tags found !" << endl;
                }
                i++;
                tags.clear();
                bad_tags.clear();                  
            }            
        }while(i<=evIter && !stop);
        cout << "EV set to : " << cam.options->ev << endl;
        cam.stopVideo();
        return 0;
    }

    void ExpoTest(lccv::PiCamera& cam, cv::Mat raw, cv::Mat image, cv::Mat sortie, Vision::io::fisheye_maps cartes){
        const float minEv = -0.5;           //Constantes car limites de la caméra
        const float maxEv = 2.0;
        const float minShutter = 1000.0;
        const float maxShutter = 10000.0;
        float evStep = 0.1;
        float shutterStep = 1000.0;
        int evIter = (int)((maxEv-minEv)/evStep);
        int shutterIter = (int)((maxShutter-minShutter)/shutterStep);

        vector<double> gradVect;
        vector<float> shutterVect;
        vector<float> evVect;

        string fPath = "AutoExpo/image_test_";
        string csvPath = "AutoExpo/values.csv";
        ofstream flux(csvPath);
        if(flux.is_open()) printf("File opened\n");
        flux << "shutter,ev,grad\n";         

        bool changed = false;
        
        for(int i = 0; i<=evIter; i){
            for(int j = 0; j<=shutterIter; j){
                if(changed == false){
                    printf("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n");
                    printf("i = %d\tj = %d\n",i,j);
                    printf("HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH\n");

                    cam.stopVideo();
                    cam.options->ev = maxEv-i*evStep;
                    cam.options->shutter = maxShutter-j*shutterStep;
                    cam.startVideo();
                    changed = true;
                }

                if (!cam.getVideoFrame(raw, 100)) {
                    fprintf(stderr, "ERR/Capture/Timeout\n");
                    fflush(stderr);
                } 
                else {
                    io::appliqueFisheye(raw, image, cartes);
                    resize(image , sortie, Size(1440,1080));
                    gradVect.push_back(io::gradientMagnitude(sortie));
                    shutterVect.push_back(cam.options->shutter);
                    evVect.push_back(cam.options->ev);                             
                    printf("Grad. Mag. : %f\n",gradVect.back());
                    printf("EV : %.1f\n",evVect.back());
                    printf("Shutter : %f\n\n",shutterVect.back());

                    ostringstream num, shutInt;
                    num.width(3);
                    num.fill('0');
                    num << i*shutterIter+j;
                    shutInt.width(5);
                    shutInt.fill('0');
                    shutInt << (int)shutterVect.back();
                    stringstream evFloat, gradFloat;
                    evFloat << fixed << setprecision(1) << evVect.back();
                    gradFloat << fixed << setprecision(2) << gradVect.back();

                    string path = fPath+num.str()+"__shutter_"+shutInt.str()+"__ev_"+evFloat.str()+"__grad_"+gradFloat.str()+"__"+".png";
                    printf("i = %d\tj = %d\n",i,j);
                    cout << path << endl;
                    imwrite(path, sortie);

                    flux << shutInt.str()+","+evFloat.str()+","+gradFloat.str()+"\n";

                    j++;
                    changed = false;                  
                } 
            }
            i++;
           
        }
        flux.close();
    }
}