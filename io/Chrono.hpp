#ifndef __VISION__IO_CHRONO__ 
#define __VISION__IO_CHRONO__ 

#include <string>
#include <iostream>
#include <iomanip>
#include <opencv2/core.hpp>
#include <vector>
#include <map>

int const FUNCTION_NAME_MAX_SIZE =  20;
int const FIELD_NAME_MAX_SIZE = 17;

using namespace std;

enum timestamp_type{
    START,
    STOP,
};

typedef struct timestamp{
    int64 tick;
    string name;
    timestamp_type type;    
}timestamp;

typedef struct funcTime{
    string name;
    double fTime;
    int frameNum;
}funcTime;



namespace Vision::io {

class Chrono {
    public:
        Chrono();
        virtual void startChrono();
        virtual void stopChrono();
        virtual void inChrono(string name);
        virtual void outChrono(string name);
        virtual void displayChrono();
        virtual int getFrameNum();

    private:
        int64 start_tick;
        int64 end_tick;
        int frameNb;
        vector<timestamp> stamps;
        map<string, vector<funcTime>> funcStats;
        virtual void fillFunc();

};


}



#endif