#ifndef __VISION__IO__
#define __VISION__IO__

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;
namespace Vision::io {

    /**
     * @brief Génère l'histogramme des couleurs de l'image. 
     * Ne fonctionne qu'en BGR
     * 
     * @param image 
     * @return l'histogramme
     */
    Mat histogramme (Mat image);
    
    /**
     * @brief Calcule le pourcentage de pixels à la valeure maximale.
     * @param image Image
     * @return Pourcentage de l'image à la valeur maximale
    */
    float histogrammeValeurs (Mat image);

    /**
     * @brief Calcule la magnitude moyenne du gradient de l'image
     * @param image Image
     * @return Magnitude moyenne du gradient de l'image
    */
    double gradientMagnitude(Mat image);

    /**
     * TODO
    */
    double distanceColorsLAB(Scalar colorBGR1, Scalar colorBGR2);

    /**
     * TODO
    */
    double distanceColorsHSV(Scalar colorBGR1, Scalar colorBGR2);
    
    /**
     * TODO
    */
    void filterCLAHE(Mat image, double clipLimit = 4.0, Size gridSize = Size(8,8) );

    /**
     * TODO
    */
    void filterSharp(Mat image, double strength = 1.0);    
    
    /**
     * TODO
    */
    void filterHistEq(Mat image, bool perChannel = false);

}

#endif