#include "Chrono.hpp"

using namespace std;
namespace Vision::io{
    Chrono::Chrono(){
        start_tick = 0;
        end_tick = 0;
        frameNb = 0;
    };

    void Chrono::startChrono(){
        start_tick = cv::getTickCount();
        stamps.clear();
    };

    void Chrono::stopChrono(){
        end_tick = cv::getTickCount();
        (frameNb)++;
    };

    void Chrono::inChrono(string nom){
        timestamp stamp;
        stamp.tick = cv::getTickCount();
        stamp.type = START;
        if(nom.size() > FUNCTION_NAME_MAX_SIZE){
            nom.resize(FUNCTION_NAME_MAX_SIZE);
        }
        stamp.name = nom;
        stamps.push_back(stamp);
    };

    void Chrono::outChrono(string nom){
        timestamp stamp;
        stamp.tick = cv::getTickCount();
        stamp.type = STOP;
        stamp.name = nom;
        stamps.push_back(stamp);
    };

    int Chrono::getFrameNum(){
        return frameNb;
    };

    void Chrono::fillFunc(){
        string funcName;
        for(int i = 0; i<stamps.size(); i++){
            if(stamps[i].type == START){
                funcTime funcT;
                funcName = stamps[i].name;
                for(int j = i+1; j<stamps.size(); j++){
                    if((stamps[j].name == funcName) && (stamps[j].type == STOP)){
                        funcT.name = funcName;
                        funcT.frameNum = frameNb;
                        funcT.fTime = (stamps[j].tick-stamps[i].tick)/cv::getTickFrequency();
                        
                        if(funcStats.count(funcT.name) == 0){
                            funcStats.insert({funcT.name, vector<funcTime>()});
                        }                            
                        funcStats.at(funcT.name).push_back(funcT);
                        break;
                    }
                }
                
            }
        }
    };

    void Chrono::displayChrono(){
        cout << "---------------------------   Time analysis   ---------------------------" << endl;
        double frameTime = (end_tick - start_tick)/cv::getTickFrequency();
        double fps = 1/frameTime;
        cout << "Frame number : " << frameNb << endl;
        cout << "Frame time : " << frameTime*1000.0 << "ms" << endl;
        cout << "Frame per second : " << fps << endl;

        if(frameNb > 1){  //Evite de biaiser les donnees avec l'initialisation
            fillFunc();
            cout << "|-------------------|----------------|----------------|----------------|" << endl;
            cout << "| Function          | Last [ms]      | Mean [ms]      | Std. Dev. [ms] |" << endl;
            cout << "|-------------------|----------------|----------------|----------------|" << endl;
            for(auto itr = funcStats.begin(); itr != funcStats.end(); itr++){
                double mean = 0.0;
                double stdv = 0.0;
                for(int i = 0; i<itr->second.size(); i++){
                    mean += itr->second[i].fTime;
                }
                mean /= itr->second.size();
                for(int i = 0; i<itr->second.size(); i++){
                    stdv += (itr->second[i].fTime - mean) * (itr->second[i].fTime - mean);
                }
                stdv = sqrt(stdv/itr->second.size());
                string strActive;
                if(itr->second.back().frameNum == frameNb) strActive = "";
                else strActive = "*";
                cout << "|" << right << setfill(' ') << setw(FUNCTION_NAME_MAX_SIZE-1) << strActive+itr->first << "|" << right << setfill(' ') << setw(FIELD_NAME_MAX_SIZE-1) << itr->second.back().fTime*1000.0 << "|" << right << setfill(' ') << setw(FIELD_NAME_MAX_SIZE-1) << mean*1000.0 << "|" << right << setfill(' ') << setw(FIELD_NAME_MAX_SIZE-1) << stdv*1000.0 << "|" <<endl;
                cout << "|-------------------|----------------|----------------|----------------|" << endl;
            }
        } 
    }
}