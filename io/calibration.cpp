#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <fstream>

#include "calibration.hpp"

using namespace std;

namespace Vision::io {

    calibration charge (string fichier){
        calibration C;
        cv::FileStorage fs(fichier, cv::FileStorage::READ);

        // Caméra
        fs["resolution"] >> C.resolution;
        fs["camera"] >> C.camera;
        fs["dist_coeffs"] >> C.dist_coeffs;
        fs["perspective"] >> C.perspective;
        fs["position"] >> C.position;
        
        // Méthode d'autoexpo
        fs["auto_expo"] >> C.expoMethode;

        // Paramètres caméra
        fs["brightness"] >> C.cameraParams.brightness;
        fs["contrast"] >> C.cameraParams.contrast;
        fs["ev"] >> C.cameraParams.ev;
        fs["gain"] >> C.cameraParams.gain;
        fs["saturation"] >> C.cameraParams.saturation;
        fs["sharpness"] >> C.cameraParams.sharpness;
        fs["shutter"] >> C.cameraParams.shutter;

        // Paramètres plantes
        fs["terrain_coeffA"] >> C.plantsParams.terrain.coeffA;
        fs["terrain_coeffB"] >> C.plantsParams.terrain.coeffB;
        fs["terrain_hue"][0] >> C.plantsParams.terrain.hue[0];
        fs["terrain_hue"][1] >> C.plantsParams.terrain.hue[1];
        fs["terrain_sat"][0] >> C.plantsParams.terrain.sat[0];
        fs["terrain_sat"][1] >> C.plantsParams.terrain.sat[1];
        fs["terrain_val"][0] >> C.plantsParams.terrain.val[0];
        fs["terrain_val"][1] >> C.plantsParams.terrain.val[1];
        fs["jardi_coeffA"] >> C.plantsParams.jardi.coeffA;
        fs["jardi_coeffB"] >> C.plantsParams.jardi.coeffB;
        fs["jardi_hue"][0] >> C.plantsParams.jardi.hue[0];
        fs["jardi_hue"][1] >> C.plantsParams.jardi.hue[1];
        fs["jardi_sat"][0] >> C.plantsParams.jardi.sat[0];
        fs["jardi_sat"][1] >> C.plantsParams.jardi.sat[1];
        fs["jardi_val"][0] >> C.plantsParams.jardi.val[0];
        fs["jardi_val"][1] >> C.plantsParams.jardi.val[1];
        fs["jardi_scan_depth"] >> C.plantsParams.jardi.scan_depth;
        fs["jardi_scan_threshold"] >> C.plantsParams.jardi.scan_threshold;

        // Paramètres filtres
        fs["filter_CLAHE_do"] >> C.filter_CLAHE_do;
        fs["filter_CLAHE_clipLimit"] >> C.filter_CLAHE_clipLimit;
        fs["filter_CLAHE_gridSize"] >> C.filter_CLAHE_gridSize;
        fs["filter_sharp_do"] >> C.filter_sharp_do;
        fs["filter_sharp_strength"] >> C.filter_sharp_strength;

        // Equipe
        fs["equipe"] >> C.equipe;

        // Logs
        fs["log_stdout"] >> C.logStdout;

        fs.release();
        return C;
    }

    void sauvegarde (string fichier, calibration C){
        cv::FileStorage fs(fichier, cv::FileStorage::WRITE);

        // Caméra
        fs << "resolution" << C.resolution;
        fs << "camera" << C.camera;
        fs << "dist_coeffs" << C.dist_coeffs;
        fs << "perspective" <<  C.perspective;
        fs << "position" <<  C.position;
        
        // Méthode d'autoexpo
        fs << "auto_expo" <<  C.expoMethode;

        // Paramètres caméra
        fs << "brightness" <<  C.cameraParams.brightness;
        fs << "contrast" <<  C.cameraParams.contrast;
        fs << "ev" <<  C.cameraParams.ev;
        fs << "gain" <<  C.cameraParams.gain;
        fs << "saturation" <<  C.cameraParams.saturation;
        fs << "sharpness" <<  C.cameraParams.sharpness;
        fs << "shutter" <<  C.cameraParams.shutter;

        // Paramètres plantes
        fs << "terrain_coeffA" <<  C.plantsParams.terrain.coeffA;
        fs << "terrain_coeffB" <<  C.plantsParams.terrain.coeffB;
        fs << "terrain_hue" <<  Point2i(C.plantsParams.terrain.hue[0], C.plantsParams.terrain.hue[1]);
        fs << "terrain_sat" <<  Point2i(C.plantsParams.terrain.sat[0], C.plantsParams.terrain.sat[1]);
        fs << "terrain_val" <<  Point2i(C.plantsParams.terrain.val[0], C.plantsParams.terrain.val[1]);
        fs << "jardi_coeffA" <<  C.plantsParams.jardi.coeffA;
        fs << "jardi_coeffB" <<  C.plantsParams.jardi.coeffB;
        fs << "jardi_hue" <<  Point2i(C.plantsParams.jardi.hue[0], C.plantsParams.jardi.hue[1]);
        fs << "jardi_sat" <<  Point2i(C.plantsParams.jardi.sat[0], C.plantsParams.jardi.sat[1]);
        fs << "jardi_val" <<  Point2i(C.plantsParams.jardi.val[0], C.plantsParams.jardi.val[1]);
        fs << "jardi_scan_depth" << C.plantsParams.jardi.scan_depth;
        fs << "jardi_scan_threshold" << C.plantsParams.jardi.scan_threshold;

        // Paramètres filtres
        fs << "filter_CLAHE_do"<< C.filter_CLAHE_do;
        fs << "filter_CLAHE_clipLimit"<< C.filter_CLAHE_clipLimit;
        fs << "filter_CLAHE_gridSize"<< C.filter_CLAHE_gridSize;
        fs << "filter_sharp_do"<< C.filter_sharp_do;
        fs << "filter_sharp_strength"<< C.filter_sharp_strength;

        // Equipe
        fs << "equipe" <<  C.equipe;

        // Logs
        fs << "log_stdout" << C.logStdout;
        fs.release();
    }

    void affiche (calibration C) {
        printf("----- Equipe : %d -----\n", C.equipe);
        printf("----- Caméra : -----\n");
        cout << "Résolution de calibration : " << C.resolution << "\n"
             << "Matrice caméra : \n" << C.camera << "\n\n"
             << "Coefficients de distance : \n" << C.dist_coeffs << "\n\n"
             << "Position de la caméra de base : " << C.position << "\n"
             << "Perspective de base : \n" << C.perspective << "\n"
             << endl;
        printf("----- Calibration : -----\n");     
        cout << "Méthode auto-expo : " << C.expoMethode  << "\n"
             << "Paramètres de la caméra : \n" 
             << "\tBrightness : " << C.cameraParams.brightness 
             << "\t\tContrast : " << C.cameraParams.contrast
             << "\t\tEV : " << C.cameraParams.ev
             << "\n\tGain : " << C.cameraParams.gain
             << "\t\tSaturation : " << C.cameraParams.saturation
             << "\t\tSharpness : " << C.cameraParams.sharpness
             << "\n\tShutter : " << C.cameraParams.shutter 
             << endl;
        printf("---- Plantes : ----\n");
        cout << "Paramètres des plantes du terrain : \n"
             << "\tCoeff. A : " << C.plantsParams.terrain.coeffA
             << "\tCoeff. B : " << C.plantsParams.terrain.coeffB
             << "\tHue : [" << C.plantsParams.terrain.hue[0] << ";" << C.plantsParams.terrain.hue[1] << "]"
             << "\tSat : [" << C.plantsParams.terrain.sat[0] << ";" << C.plantsParams.terrain.sat[1] << "]"
             << "\tVal : [" << C.plantsParams.terrain.val[0] << ";" << C.plantsParams.terrain.val[1] << "]"
             << "\nParamètres des plantes des jardinières : \n"
             << "\tCoeff. A : " << C.plantsParams.jardi.coeffA
             << "\tCoeff. B : " << C.plantsParams.jardi.coeffB
             << "\tHue : [" << C.plantsParams.jardi.hue[0] << ";" << C.plantsParams.jardi.hue[1] << "]"
             << "\tSat : [" << C.plantsParams.jardi.sat[0] << ";" << C.plantsParams.jardi.sat[1] << "]"
             << "\tVal : [" << C.plantsParams.jardi.val[0] << ";" << C.plantsParams.jardi.val[1] << "]"
             << "\tScan depth : " << C.plantsParams.jardi.scan_depth
             << "\tScan threshold : " << C.plantsParams.jardi.scan_threshold
             << endl;
        printf("---- Filter : ----\n");
        cout << "Filtre CLAHE : " << C.filter_CLAHE_do << endl
             << "\tclipLimit : " << C.filter_CLAHE_clipLimit
             << "\tgridSize : " << C.filter_CLAHE_gridSize << endl
             << "Filtre Sharp : " << C.filter_sharp_do << endl
             << "\tstrength : " << C.filter_sharp_strength
             << endl;
        printf("------------------------\n");
    }

    void loadConfigCamera(calibration Calibration, lccv::PiCamera *cam){
        cam->options->video_height = Calibration.resolution.height;
        cam->options->video_width = Calibration.resolution.width;
        cam->options->framerate = 5;
        cam->options->photo_height = Calibration.resolution.height;
        cam->options->photo_width = Calibration.resolution.width;

        cam->options->brightness = Calibration.cameraParams.brightness;
        cam->options->contrast = Calibration.cameraParams.contrast;
        cam->options->ev = Calibration.cameraParams.ev;
        cam->options->gain = Calibration.cameraParams.gain;
        cam->options->saturation = Calibration.cameraParams.saturation;
        cam->options->sharpness = Calibration.cameraParams.sharpness;
        cam->options->shutter = Calibration.cameraParams.shutter; 
    }

    void loadConfigPlantes(calibration Calibration, plateau::PlantsDetectionParams *plantesParams){
        plantesParams->terrain.hue = Calibration.plantsParams.terrain.hue;
        plantesParams->terrain.sat = Calibration.plantsParams.terrain.sat;
        plantesParams->terrain.val = Calibration.plantsParams.terrain.val;
        plantesParams->terrain.coeffA = Calibration.plantsParams.terrain.coeffA;
        plantesParams->terrain.coeffB = Calibration.plantsParams.terrain.coeffB;
        plantesParams->jardi.hue = Calibration.plantsParams.jardi.hue;
        plantesParams->jardi.sat = Calibration.plantsParams.jardi.sat;
        plantesParams->jardi.val = Calibration.plantsParams.jardi.val;
        plantesParams->jardi.coeffA = Calibration.plantsParams.jardi.coeffA;
        plantesParams->jardi.coeffB = Calibration.plantsParams.jardi.coeffB;
        plantesParams->jardi.scan_depth = Calibration.plantsParams.jardi.scan_depth;
        plantesParams->jardi.scan_threshold = Calibration.plantsParams.jardi.scan_threshold;
    }

    fisheye_maps calculeFisheye (calibration C){
        Mat matriceTriche;
        fisheye_maps out;

        C.camera.copyTo(matriceTriche);

        // Askip ça permet d'avoir l'image entière 
        matriceTriche.at<float>(0, 0) /= 2;
        matriceTriche.at<float>(1, 1) /= 2;

        fisheye::initUndistortRectifyMap(
            C.camera, C.dist_coeffs, Mat::eye(Size(3,3), CV_32F), 
            matriceTriche, C.resolution, CV_32F, 
            out.map1, out.map2
        );
        return out;
    }

    void appliqueFisheye (cv::Mat image, cv::Mat& sortie, fisheye_maps fmaps){      
        remap(image, sortie, fmaps.map1, fmaps.map2, INTER_LINEAR, BORDER_CONSTANT);
    }
}