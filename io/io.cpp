#include "io.hpp"
#include <opencv2/highgui.hpp>

namespace Vision::io {

    Mat histogramme (Mat image) {
        //https://docs.opencv.org/4.x/d8/dbc/tutorial_histogram_calculation.html -- Pourquoi ne pas voler ce qui marche ?
        vector<Mat> bgr_planes;
        split( image, bgr_planes );
        int histSize = 256;
        float range[] = { 0, 256 }; //the upper boundary is exclusive
        const float* histRange[] = { range };
        bool uniform = true, accumulate = false;
        Mat b_hist, g_hist, r_hist;
        calcHist( &bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, histRange, uniform, accumulate );
        calcHist( &bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, histRange, uniform, accumulate );
        calcHist( &bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, histRange, uniform, accumulate );
        int hist_w = 512, hist_h = 400;
        int bin_w = cvRound( (double) hist_w/histSize );
        Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );
        normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
        normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
        normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
        for( int i = 1; i < histSize; i++ )
        {
            line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ),
                Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
                Scalar( 255, 0, 0), 2, 8, 0  );
            line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ),
                Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
                Scalar( 0, 255, 0), 2, 8, 0  );
            line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ),
                Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
                Scalar( 0, 0, 255), 2, 8, 0  );
        }

        return histImage;

    }

    float histogrammeValeurs (Mat image) {
        //https://docs.opencv.org/4.x/d8/dbc/tutorial_histogram_calculation.html -- Pourquoi ne pas voler ce qui marche ?
        Mat image_hsv;
        cvtColor(image, image_hsv,COLOR_BGR2HSV);
        vector<Mat> hsv_planes;
        split( image_hsv, hsv_planes );        
        int histSize = 256;
        float range[] = { 0, 256 }; //the upper boundary is exclusive
        const float* histRange[] = { range };
        bool uniform = true, accumulate = false;        
        Mat v_hist;        
        calcHist( &hsv_planes[2], 1, 0, Mat(), v_hist, 1, &histSize, histRange, uniform, accumulate );        
        float percentage[histSize];
        float imageArea = image.rows*image.cols;
        for(int i = 0; i<histSize; i++){
            percentage[i] = v_hist.at<float_t>(i,1)/imageArea*100.0;            
        }
        printf("Element 254 : %f\n",percentage[254]);        
        return percentage[254];        
    }

    double gradientMagnitude(Mat image){ 
        double sumGrad;
        double nbPixels;
        Mat image_grey, sob_x, sob_y, abs_sob_x, abs_sob_y, grad;
        cvtColor(image, image_grey, COLOR_BGR2GRAY);
        GaussianBlur(image_grey, image_grey, Size(3,3), 0, 0);
        Sobel(image_grey, sob_x, CV_16S, 1, 0, 3);
        Sobel(image_grey, sob_y, CV_16S, 0, 1, 3);
        convertScaleAbs(sob_x, abs_sob_x);
        convertScaleAbs(sob_y, abs_sob_y);
        addWeighted(abs_sob_x, 0.5, abs_sob_y, 0.5, 0, grad);
        sumGrad = sum(grad)[0];
        nbPixels = grad.cols*grad.rows;
        return sumGrad/nbPixels;
    }

    double distanceColorsLAB(Scalar colorBGR1, Scalar colorBGR2){
        Mat col1(1, 1, CV_8UC3, colorBGR1);
        Mat col2(1, 1, CV_8UC3, colorBGR2);
        cvtColor(col1, col1, COLOR_BGR2Lab);
        cvtColor(col2, col2, COLOR_BGR2Lab);
        Scalar colorLab1 = Scalar((int)col1.at<Vec3b>(0,0)[0], (int)col1.at<Vec3b>(0,0)[0], (int)col1.at<Vec3b>(0,0)[0] );
        Scalar colorLab2 = Scalar((int)col2.at<Vec3b>(0,0)[0], (int)col2.at<Vec3b>(0,0)[0], (int)col2.at<Vec3b>(0,0)[0] );
        Scalar diff = colorLab1 - colorLab2;       
        return sqrt(diff[0]*diff[0]+diff[1]*diff[1]+diff[2]*diff[2]);
    }

    double distanceColorsHSV(Scalar colorBGR1, Scalar colorBGR2){
        Mat col1(1, 1, CV_8UC3, colorBGR1);
        Mat col2(1, 1, CV_8UC3, colorBGR2);
        cvtColor(col1, col1, COLOR_BGR2HSV);
        cvtColor(col2, col2, COLOR_BGR2HSV);
        Scalar colorLab1 = Scalar((int)col1.at<Vec3b>(0,0)[0], (int)col1.at<Vec3b>(0,0)[0], (int)col1.at<Vec3b>(0,0)[0] );
        Scalar colorLab2 = Scalar((int)col2.at<Vec3b>(0,0)[0], (int)col2.at<Vec3b>(0,0)[0], (int)col2.at<Vec3b>(0,0)[0] );
        Scalar diff = colorLab1 - colorLab2;       
        return abs(diff[0]);
    }

    void filterCLAHE(Mat image, double clipLimit, Size gridSize ){
        Mat channel;
        cvtColor(image, image, COLOR_BGR2Lab);
        extractChannel(image, channel, 0);
        Ptr<CLAHE> clahe = createCLAHE();
        clahe->setClipLimit(clipLimit);
        clahe->setTilesGridSize(gridSize);
        clahe->apply(channel, channel);
        insertChannel(channel, image, 0);
        cvtColor(image, image, COLOR_Lab2BGR);
        channel.release();
    }

    void filterSharp(Mat image, double strength){
        Mat sharp_ker = (Mat_<double>(3, 3) );
        for(int i = 0; i<3; i++){
            for(int j = 0; j<3; j++){
                sharp_ker.at<double>(i,j) = -strength;
            }            
        }
        sharp_ker.at<double>(1,1) = strength*8.0+1.0;
        filter2D(image, image, -1, sharp_ker);
    }

    void filterHistEq(Mat image, bool perChannel){
        if(perChannel){
            vector<Mat> image_channels;
            split(image, image_channels);
            equalizeHist(image_channels[0], image_channels[0]);
            equalizeHist(image_channels[1], image_channels[1]);
            equalizeHist(image_channels[2], image_channels[2]);
            merge(image_channels, image);
        }
        else{
            cvtColor(image, image, COLOR_BGR2Lab);
            Mat channel;
            extractChannel(image, channel, 0);
            equalizeHist(channel, channel);
            insertChannel(channel, image, 0);
            cvtColor(image, image, COLOR_Lab2BGR);
            channel.release();
        }
        
    }
}