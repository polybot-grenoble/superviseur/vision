#ifndef __VISION__IO_CALIB__
#define __VISION__IO_CALIB__

#include "../plateau/Zone.hpp"
#include <lccv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

namespace Vision::io {

    typedef enum { EQUIPE_UNKNOWN = 0, EQUIPE_BLEUE = 1, EQUIPE_JAUNE = 2 } EQUIPE;

    typedef enum {NONE = 0, BURNT, GRADIANT, TAGS} AutoExpo;

    typedef struct CameraParam{
        float brightness;
        float contrast;
        float ev;
        float gain;
        float saturation;
        float sharpness; 
        float shutter;
    } CameraParam;

    typedef struct calibration{        
        // ---- Partie générée par l'autofocus ---- //
        Size resolution;    // Resolution de la caméra
        Mat camera;         // Matrice de déformation de la caméra
        Mat dist_coeffs;    // Coefficients de distance
        Mat perspective;    // Matrice de transformation de la perspective basique
        Point3f position;   // Position dans l'espace de la caméra 
        
        // Méthode d'autoexposition
        AutoExpo expoMethode;

        // Paramètres camera
        CameraParam cameraParams;

        // Paramètres plantes
        plateau::PlantsDetectionParams plantsParams;

        // Paramètres filtres
        bool filter_CLAHE_do;   double filter_CLAHE_clipLimit;   Size filter_CLAHE_gridSize;
        bool filter_sharp_do;   double filter_sharp_strength;

        // Notre équipe
        EQUIPE equipe;

        // Logs
        bool logStdout;

    } calibration;

    typedef struct _fisheye_maps {
        Mat map1; Mat map2;
    } fisheye_maps;
    
    /**
     * Charge la configuration issue de la calibration
     * 
     * @param fichier chemin vers le fichier de sortie
     * 
     * @return La calibration chargée
    */
    calibration charge (std::string fichier);

    /**
     * Cette fonction enregistre les matrices issues de la
     * calibration dans un fichier yaml
     * 
     * @param fichier chemin vers le fichier de sortie
     * @param C la calibration
     * 
    */
    void sauvegarde (std::string, calibration C);

    /**
     * Affiche dans la console la calibration
     * 
     * @param C La calibration
    */
    void affiche (calibration C);

    /**
     * TODO
    */
    void loadConfigCamera(calibration Calibration, lccv::PiCamera *cam);

    /**
     * TODO
    */
    void loadConfigPlantes(calibration Calibration, plateau::PlantsDetectionParams *plantesParams);
    
    /**
     * Calcule les cartes de transformation pour les lentilles fisheye
    */
    fisheye_maps calculeFisheye (calibration C);

    /**
     * Applique la distorsion pour une optique fisheye
    */
    void appliqueFisheye (Mat image, Mat& sortie, fisheye_maps fmaps);
    
}

#endif