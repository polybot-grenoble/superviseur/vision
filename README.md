# Projet Vision Superviseur

## Introduction

### Un petit mot

Ce répertoire contient la partie de traitement de l'image
du superviseur. 

Le programme est construit sur la base de la bibliothèque OpenCV à l'aide du langage C++.

Bien que C++ fasse peur de part son nom, avec de bonnes bases en C, il est assez simple de s'adapter au petites différences offertes par C++.

De plus, le développement se fait sous __Linux__, car on veut utiliser notre programme sur un Raspi 4, on ne va pas tenter de configurer Visual Studio pour exporter vers linux aarch64. Si vous tenez à développer sous Windows, configurez WSL**2** avec l'addon **WSLg** (natif sur Windows 11).

Pour mieux comprendre le code, je vous invite d'abord à lire l'introduction des concepts clé d'OpenCV (en nglois malheureusemment)
dans leur [documentation officielle](https://docs.opencv.org/4.x/de/d7a/tutorial_table_of_content_core.html) de _Mat - The Basic Image Container_ à _Changing the contrast and brightness of an image!_.

### But du programme

Le programme se base sur le règlement de la coupe de robotique de France édition 2023.

Le programme se doit de:

 - Détecter l'aire de jeu à l'aide des tags ArUco de l'aire de jeu
 - Détecter les balises des robots pour leur indiquer leur position
 - Détecter les éléments de jeu et donner leur localisation s'ils sont accessibles

## Plan

1. [Installation des dépendances](#installation-des-dépendances)
    1. [Dépendances génériques](#dépendances-génériques) 
    2. [FFMPEG sous 22.04](#ffmpeg-sous-2204)
    3. [Libcamera](#libcamera)
    4. [LCCV](#lccv)
    5. [Compilation d'OpenCV](#compilation-dopencv)
2. [Stucture du code](#structure-du-code)
    1. [CMake](#cmake)
    2. [Explication de l'arboréscence](#explication-de-larboréscence)
3. [Utilisation du programme](#utilisation-du-programme)
    1. [Compilation](#compilation)
    2. [Execution](#execution)
4. [Rapport Hebdomadaire](#rapport-hebdomadaire)

## Installation des dépendances

Ici le tutoriel est prévu pour Ubuntu 22.04, mais rien n'empêche d'utiliser une autre distro (Arch, Fedora, ...) tant que vous savez utiliser le gestionnaire de paquets.

Le but est de __compiler__ OpenCV sur votre machine de développement, si vous voulez développer directement sur RaspberryPi, une image Rapberry Pi OS lite contenant OpenCV pré-compilé sera fournie. A noter que l'image est prévue pour les systèmes aarch64 (Raspi 3/4).

### Dépendances génériques

Afin de compiler télécharger, compiler et installer OpenCV, vous aurez besoin d'installer `wget g++ unzip cmake libgtk2.0-dev gstreamer ffmpeg`. 

Sous Ubuntu 22.04 par exemple:
`sudo apt update && sudo apt install wget unzip cmake g++ libgtk2.0-dev libgestreamer1.0-dev ffmpeg`

Vous aurez aussi besoin de compiler et d'installer [Libcamera](#libcamera) et [LCCV](#lccv).

**NB**: Sous Ubuntu 22.04, si vous l'avez en dualboot, vous aurez besoin de vous réferer à la partie [FFMPEG sous 22.04](#ffmpeg-sous-2204) pour que FFMPEG soit détecté lors de la compilation d'OpenCV. Le problème ne se manifeste pas sous WSL2 ni sous Raspibian.

**NB**: Sous Arch (ou Manjaro, Endeavour, ...) `g++` est contenu dans le paquet `gcc`.

### FFMPEG sous 22.04

Pour une obscure raison, `pkg-config` ne trouve pas les librairies ffmpeg si ce dernier est installé avec un gestionnaire de paquet. On doit compiler FFMPEG :

```bash
# Remove ffmpeg install
sudo apt --purge autoremove ffmpeg
sudo apt install libx264-dev yasm

# Download and unpack sources
wget -O ffmpeg.zip https://github.com/FFmpeg/FFmpeg/archive/refs/heads/master.zip

# Unzip and switch into the folder
unzip ffmpeg.zip
cd ffmpeg

# Configure
./configure --enable-shared --disable-static --enable-libx264 --enable-gpl --prefix=/usr/local

# Build
make -j8

# Install
sudo make install

# Check if the environment variable is already set
$echo LD_LIBRARY_PATH

# If not, set the variable
LD_LIBRARY_PATH=/usr/local/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/libswresample.so/

# Export the variable
export LD_LIBRARY_PATH
```

**NB**: Il faut ajouter la variable dans le `~/.bashrc` pour que celle-ci persiste.

### Libcamera

Sur un raspberrypi, installez simplement `libcamera-dev`.

Pour pouvoir développer sur PC par contre, il va falloir compiler libcamera pour pouvoir 
effectuer la compilation de notre programme. Les instructions sont les suivantes 
(inspiré de la [documentation officielle](https://libcamera.org/getting-started.html)):

```sh
# Installer les dépendances
sudo apt install libyaml-dev python3-yaml python3-ply python3-jinja2 meson

# Cloner le repo libcamera
git clone https://git.libcamera.org/libcamera/libcamera.git

# Compiler
cd libcamera
meson build
sudo ninja -C build install
```

### LCCV

Pour installer LCCV, il suffit de suivre les instructions suivantes, tirées de son [GitHub](https://github.com/kbarni/LCCV):

```sh
# Clonage du repo
git clone https://github.com/kbarni/LCCV.git

# Compilation
cd LCCV
mkdir build && cd build
cmake ..
make

# Installation
sudo make install
```

**NB**: Vous pouvez accélerer le build en indiquand à make le nombre de threads de votre processeur. Exemple: si vous avez 8 threads > `make -j 8`

### Compilation d'OpenCV

Je vais simplement vous citer l'extrait de la [doc officielle](https://docs.opencv.org/4.x/d7/d9f/tutorial_linux_install.html) :

```bash
# Download and unpack sources
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.x.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.x.zip
unzip opencv.zip
unzip opencv_contrib.zip

# Create build directory and switch into it
mkdir -p build && cd build

# Configure
cmake -DOPENCV_EXTRA_MODULES_PATH=../opencv_contrib-4.x/modules ../opencv-4.x

# Build
cmake --build .
```

**NB**: Vous pouvez accélerer le build en indiquand à cmake le nombre de threads de votre processeur. Exemple: si vous avez 8 threads > `cmake --build . -- -j 8`

Ensuite, pour installer OpenCV, faites simplement `sudo make install` dans le dossier de build. 

**ATTENTION ! Ne supprimez pas tout de suite les fichiers de compilation !**

Si vous avez des problèmes de dépendances et que vous vous en rendez compte à posteriori, il vous suffira d'installer la dépendance en question et de relancer le build. CMake est suffisemment intélligent pour recompiler uniquement les parties nécessaires. Bien sûr il faudra ré-installer OpenCV avec `sudo make install` en fin de re-compilation.

## Structure du code

### CMake

Pour utiliser OpenCV, il nous est nécessaire d'utiliser CMake.

Bien evidemment, nous ne savons pas comment ça fonctionne ce programme du malin, sinon ce ne serait pas drôle HAHA (tuez-moi s'il-vous-plaît)

Ce qu'on sait et qui sera utile pour nous c'est:

 - CMake lis les règles de compilation dans les fichiers `CMakeLists.txt`
 - Pour CMake, un dossier est une bibliothèque, à ce titre :
    * Le dossier à besoin d'un `CMakeLists.txt`
    * L'importation d'un dossier se fait via `add_subdirectory(dossier)` dans un fichier `CMakeLists.txt`
    * La création d'une bibliothèque se fait via `add_library(NOM fichier1.cpp fichier2.cpp)` (en remplaçant NOM par le nom de votre bibliothèque)
    * CMake trouvera tout seul les fichiers .h et .hpp pour les codes sources précisés dans `add_library`
    * Les `CMakeLists.txt` des sous-dossiers doivent avoir pour première ligne `link_libraries(${OpenCV_LIBS})` pour importer OpenCV dans les codes sources des sous-dossiers
 - Pour lui demander de compiler l'executable en incorporant les sous-dossiers, il faut ajouter les bibliothèques dans l'instruction `target_link_libraries(Executable ${OpenCV_LIBS} Biblio1 Biblio2)` (Biblio# est le nom de la __Bibliothèque__ et non du dossier)

### Explication de l'arboréscence

 - `archive` contient des fichiers sources archivés lors du refactor (voué à disparaître)
 - `transformation` contient les fonctions permettant de préparer l'image à l'analyse et de déterminer divers paramètres pour établir le repère
 - `plateau` contient les fonctions et structures pemettant la définition d'un repère et d'exprimer les coordonnées d'éléments détectés en fonction du repère.
 - `analyse` contient les fonctions d'analyse sur une image pré-traitée
 - `io` continet les fonctions et structures permettant l'entrée/sotie de données (i.e. config, stdout, cli)
 - `exe` contient comme son nom l'indique les codes source des différents executables

## Utilisation du programme

### Compilation

Créez un dossier `build` dans la racine du projet, c'est un dossier qui sera ignoré par git, puis à l'intérieur du dossier (`cd build`), initializez le à l'aide de `cmake ..` et compilez avec `cmake --build .`

**NB**: Vous n'avez à initialiser le dossier qu'une fois du coup

### Execution

Voici la liste des executables: (à noter qu'ils sont tous préfixés de `vision-`)
    
    - `photo` Prend une photo avec libcamera. Si un fichier de calibration lui est donné, il va alors effectuer la transformation de perspective.
    - `png` Prend une image et un fichier confiuration en paramètres et retourne l'image analysée sous forme de png. 

## Rapport Hebdomadaire

## 04/11/2022 - Intégration de Libcamera

- Découverte de la caméra et de son interface
- Grande difficulté à trouver un moyen de l'intégrer
- Intégration de LCCV
- Ajout des étapes d'installation de LCCV
- Ajout des étapes d'installation de libcamera sous WSL2/Dual-boot

#### __Petit mot sur Libcamera:__

Lors du passage à Debian 11, la fondation Raspberry Pi a fait le choix de changer le
driver caméra pour se débarrasser de v4l2 alors viellissant pour un nouveau driver: libcamera.

Le problème est que libcamera est très (trop) récent, et OpenCV n'a aucun moyen de s'y interfacer.
C'est pourquoi on a envisagé de passer sous python où il existe PiCamera2 servant d'interface
entre OpenCV et libcamera. Mais le performances n'étaient absolument pas au rendez-vous, on avait
entre 1 et 2 fps alors qu'il ne s'agissait que de la première étape de pré-traitement de l'image.

Heureusement après de longues recherches, je suis tombé sur [LCCV](https://github.com/kbarni/LCCV),
qui est certes un projet peu testé mais qui fonctionne dans notre cas.

## 27/10/2022 - Création du répertoire de développement

- Bataille contre CMake pour qu'il comprenne ce qu'on veut faire
- Tests de détection ArUco et de distortion de l'image
- Création du README

## 13/10/2022 - Démarrage

- Découverte d'OpenCV
- Réflexion sur les fonctions du programme
