#include "Zone.hpp"


namespace Vision::plateau{
    // ##################   Zone   ##################

    Zone::Zone(){}

    Zone_Type Zone::getType(){
        return type;
    }

    int Zone::getID(){
        return id;
    }
    
    double Zone::getPlantNumber(){
        return plant_number;
    }
    
    void Zone::drawZoneTags(Mat image, std::vector<arucoTag> tags){}

    std::vector<arucoTag> Zone::detectTag(std::vector<arucoTag> tags){
        printf("Detecting zone that doesn't exist...\n");
        std::vector<arucoTag> a;
        return a;
    }

    void Zone::calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos){
        cout << "Called 'calibratePlants' on a default Zone!" << endl;
    }
    
    double Zone::plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display){
        cout << "Called 'plantSurface' on a default Zone!" << endl;
        return -1.0;
    }


    double Zone::plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params){
        cout << "Called 'plantNumber' on a default Zone!" << endl;
        return -1.0;
    }

    long Zone::whitePixelCounter(Mat &zoneImage, Mat &zoneMask, Mat &colorMask, Mat &val, DetectionParams params){
        Mat hue = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat sat = Mat(zoneImage.size(), CV_8U, (uchar)0); 
        Mat hueMask = Mat(zoneImage.size(), CV_8U, (uchar)0); 
        Mat satMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat valMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        // Extract Hue, Saturation & Value in separated Mat
        cvtColor(zoneImage, zoneImage, COLOR_BGR2HSV_FULL);        
        extractChannel(zoneImage, hue, 0);
        extractChannel(zoneImage, sat, 1);
        extractChannel(zoneImage, val, 2);
        // Create masks
        inRange(hue, params.hue[0], params.hue[1], hueMask);
        inRange(sat, params.sat[0], params.sat[1], satMask);
        inRange(val, params.val[0], params.val[1], valMask);
        // Combine masks
        bitwise_and(hueMask, satMask, colorMask);
        bitwise_and(colorMask, valMask, colorMask);
        bitwise_and(colorMask, zoneMask, colorMask);
        // Sum non-0 pixels using pointer access
        long sum = 0;
        uint8_t *imgData = colorMask.data;
        uint8_t *maskData = zoneMask.data;
        int width = colorMask.cols;
        int height = colorMask.rows;
        int step = colorMask.step;
        for(int i = 0; i<width; i++){
            for(int j = 0; j<height; j++){
                if(imgData[i*step +j] != 0) sum++;
            }
        }
        return sum;
    }  


    // ##################   ZoneRectangle  ##################

    ZoneRectangle::ZoneRectangle(cv::Point2f minCorner, cv::Point2f maxCorner, Zone_Type type, int id){
        this->minCorner = minCorner;
        this->maxCorner = maxCorner;
        this->type = type;
        this->id = id;
        this->plant_number = -1.0;
    }

    Zone_Type ZoneRectangle::getType(){
        return type;
    }

    void ZoneRectangle::drawZone(Mat image){
        vector<Point2f> points;
        points.push_back(minCorner);
        points.push_back(Point2f(minCorner.x, maxCorner.y));
        points.push_back(maxCorner);
        points.push_back(Point2f(maxCorner.x, minCorner.y));

        for(int i = 0; i<4; i++){
            points[i] = plateau::reel2pixel(points[i], image.cols, image.rows);
        }
        
        plateau::draw::quad(image, points, Scalar(50,255,50));
    }

    void ZoneRectangle::drawZoneTags(Mat image, std::vector<arucoTag> tags){
        ZoneRectangle::drawZone(image);
        std::vector<arucoTag> insideTags;
        insideTags = ZoneRectangle::detectTag(tags);
        Point2f anchor = plateau::reel2pixel(minCorner, image.cols, image.rows);
        std::string tag_list = "id : ";
        for(auto tag : insideTags){
            tag_list.append(to_string(tag.id));
            tag_list.append("  ");
        }
        putText(image, tag_list, anchor+Point2f(0, 30), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
    }

    std::vector<arucoTag> ZoneRectangle::detectTag(std::vector<arucoTag> tags){
        std::vector<arucoTag> inZoneTags;
        for(int i = 0; i<tags.size(); i++){
            if(    (tags[i].posReel.center.x <= maxCorner.x) 
                && (tags[i].posReel.center.x >= minCorner.x) 
                && (tags[i].posReel.center.y <= maxCorner.y) 
                && (tags[i].posReel.center.y >= minCorner.y) )
            {
                inZoneTags.push_back(tags[i]);
            }
        }
        return inZoneTags;
    }

    void ZoneRectangle::calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos){
        cout << "Template..." << endl;
    }

    double ZoneRectangle::plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display){
        // Region of interest of the image
        float zone_scale = CONSTANTES::plant_zone_scale;
        float jardi_height = CONSTANTES::jardi_heigth;
        Rect ROI_zone(  plateau::reel2pixel(space2reel(Point3f(minCorner.x, minCorner.y, jardi_height), camPos), imageIn.cols, imageIn.rows), 
                        plateau::reel2pixel(space2reel(Point3f(maxCorner.x, maxCorner.y, jardi_height), camPos), imageIn.cols, imageIn.rows)
        );        
        // Create smaller image for speed
        Mat zoneImage;
        double scale = 0.25;
        double areaScale = 1/(scale*scale);
        resize(imageIn(ROI_zone), zoneImage, Size(0,0), scale, scale, INTER_AREA);        
        // Initialize rectangular zone mask
        Mat zoneMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat colorMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat val = Mat(zoneImage.size(), CV_8U, (uchar)0);
        zoneMask.setTo(Scalar(255,255,255));
        // Count pixels in color range
        long sum = whitePixelCounter(zoneImage, zoneMask, colorMask, val, params);
        sum *= areaScale;     
        // Set masked pixels to black
        if(display){
            bitwise_not(zoneMask, zoneMask);
            bitwise_or(colorMask, zoneMask, colorMask);
            subtract(colorMask, Scalar::all(254), colorMask, colorMask);
            multiply(val, colorMask, val);
            insertChannel(val, zoneImage, 2);
            // Convert back to BGR
            cvtColor(zoneImage, zoneImage, COLOR_HSV2BGR_FULL);
            resize(zoneImage, zoneImage, ROI_zone.size(), INTER_NEAREST);

            zoneImage.copyTo(imageOut(ROI_zone));
        }
        return sum;
    }

    double ZoneRectangle::plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params){
        double area = CONSTANTES::plateau_largeur * CONSTANTES::plateau_hauteur;        
        double plantArea = ZoneRectangle::plantSurface(imageIn, imageOut, camPos, params, true);
        // Update plant number field
        plant_number = ( (100.0*plantArea/area) + 0.0 )*params.coeffA; 
        return  plant_number;
    }

    
    // ##################   ZoneCercle   ##################

    ZoneCercle::ZoneCercle(Point2f center, float radius, Zone_Type type, int id){
        this->center = center;
        this->radius = radius;
        this->type = type;
        this->id = id;
        this->plant_number = -1.0;
    }

    Zone_Type ZoneCercle::getType(){
        return type;
    }

    void ZoneCercle::drawZone(Mat image){
        
        Point2f c_center = plateau::reel2pixel(center, image.cols, image.rows);               
        float scale = CONSTANTES::image_scale;
        
        plateau::draw::circle(image, c_center, radius*scale, Scalar(255,50,50));
    }

    void ZoneCercle::drawZoneTags(Mat image, std::vector<arucoTag> tags){
        ZoneCercle::drawZone(image);
        std::vector<arucoTag> insideTags;
        insideTags = ZoneCercle::detectTag(tags);
        Point2f anchor = plateau::reel2pixel(center-Point2f(radius,radius), image.cols, image.rows);
        std::string tag_list = "id : ";
        for(auto tag : insideTags){
            tag_list.append(to_string(tag.id));
            tag_list.append("  ");
        }
        putText(image, tag_list, anchor+Point2f(0, 35), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
    }

    std::vector<arucoTag> ZoneCercle::detectTag(std::vector<arucoTag> tags){
        std::vector<arucoTag> inZoneTags;
        for(int i = 0; i<tags.size(); i++){
            if(norm(tags[i].posReel.center-center)<= radius){
                inZoneTags.push_back(tags[i]);
            }
        }
        return inZoneTags;
    }

    void ZoneCercle::calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos){
        int hue_midpoint = 70;
        int hue_width = 10;
        int hue_scan_width = 7;
        double coverageThreshold1 = 13000;
        double coverageThreshold2 = 16000;  
        double surface;
        DetectionParams params;
        params.hue = Scalar(hue_midpoint-hue_width, hue_midpoint+hue_width);
        params.sat = Scalar(60, 255);
        params.val = Scalar(30, 225);
        cout << "Phase 1..." << endl;

        surface = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, false);
        while( (surface < coverageThreshold1) && hue_width < 25){
            cout << "Midpoint : " << hue_midpoint << "\tWidth : " << hue_width;
            cout << "\tCurrent range : " << hue_midpoint-hue_width << " to " << hue_midpoint+hue_width;
            cout << "\tSurface : " << surface << endl;
            hue_width += 1;
            params.hue = Scalar(hue_midpoint-hue_width, hue_midpoint+hue_width);
            surface = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, false);
        }
        double maxSurface = 0.0;
        int maxMidpoint = hue_midpoint;
        cout << "Phase 2..." << endl;
        for(int i = hue_midpoint-hue_scan_width; i <= hue_midpoint+hue_scan_width; i++){
            params.hue = Scalar(i-hue_width, i+hue_width);
            surface = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, false);
            cout << "Midpoint : " << i << "\tWidth : " << hue_width;
            cout << "\tSurface : " << surface << endl;
            if(surface > maxSurface){
                maxSurface = surface;
                maxMidpoint = i; 
            }
            cout << "Max : " << maxMidpoint << endl;
        }
        hue_midpoint = maxMidpoint;
        cout << "Phase 3..." << endl;
        surface = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, false);
        while((surface < coverageThreshold2 ) && hue_width < 40){
            cout << "Midpoint : " << hue_midpoint << "\tWidth : " << hue_width;
            cout << "\tCurrent range : " << hue_midpoint-hue_width << " to " << hue_midpoint+hue_width;
            cout << "\tSurface : " << surface << endl;
            hue_width += 1;
            params.hue = Scalar(hue_midpoint-hue_width, hue_midpoint+hue_width);
            surface = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, false);
        }
        cout << "Range found : " << hue_midpoint-hue_width << " to " << hue_midpoint+hue_width << endl;
    }

    double ZoneCercle::plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display ){
        // Region of interest of the image
        float zone_scale = CONSTANTES::plant_zone_scale;
        Point2f bound_min = center-Point2f(radius*zone_scale, radius*zone_scale);
        Point2f bound_max = center+Point2f(radius*zone_scale, radius*zone_scale);
        float plant_height = CONSTANTES::plant_heigth;
        Rect ROI_zone(  plateau::reel2pixel(space2reel(Point3f(bound_min.x, bound_min.y, plant_height), camPos), imageIn.cols, imageIn.rows), 
                        plateau::reel2pixel(space2reel(Point3f(bound_max.x, bound_max.y, plant_height), camPos), imageIn.cols, imageIn.rows)
        );
        if( (ROI_zone.x == 0) || (ROI_zone.y == 0)){
            for(int i = 0; i<30; i++){
                printf("WARNING WARNING WARNING WARNING WARNING WARNING\nROI_zone as a scale of 0\nReturning 1.0 to avoid crashing\n");
                return 1.0;
            }
        }
        // Create smaller image for speed
        Mat zoneImage;
        double scale = 0.25;
        double areaScale = 1/(scale*scale);
        resize(imageIn(ROI_zone), zoneImage, Size(0,0), scale, scale, INTER_AREA);
        // Initialize circular zone mask
        Mat zoneMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat colorMask = Mat(zoneImage.size(), CV_8U, (uchar)0);
        Mat val = Mat(zoneImage.size(), CV_8U, (uchar)0);
        circle(zoneMask, Point2i(zoneMask.cols/2, zoneMask.rows/2), zoneMask.cols/2, Scalar(255,255,255), FILLED, LINE_4);
        // Count pixels in color range
        long sum = whitePixelCounter(zoneImage, zoneMask, colorMask, val, params);
        sum *= areaScale;   //Adjusting for smaller image scale;
        // Set masked pixels to black
        if(display){
            bitwise_not(zoneMask, zoneMask);
            bitwise_or(colorMask, zoneMask, colorMask);
            subtract(colorMask, Scalar::all(254), colorMask, colorMask);
            multiply(val, colorMask, val);
            insertChannel(val, zoneImage, 2);
            // Convert back to BGR
            cvtColor(zoneImage, zoneImage, COLOR_HSV2BGR_FULL);
            resize(zoneImage, zoneImage, ROI_zone.size(), INTER_NEAREST);
             
            zoneImage.copyTo(imageOut(ROI_zone));
        }
        return sum;
    }

    double ZoneCercle::plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params){
        double area = CONSTANTES::plateau_largeur * CONSTANTES::plateau_hauteur;        
        double plantArea = ZoneCercle::plantSurface(imageIn, imageOut, camPos, params, true);
        // Update plant number field
        plant_number = ( (100.0*plantArea/area) + 0.0 )*params.coeffA; 
        return  plant_number;
    }
}