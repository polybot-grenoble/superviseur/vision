#ifndef __VISION__PLATEAU_DETECTIONWORK__ 
#define __VISION__PLATEAU_DETECTIONWORK__ 

#include "../io/io.hpp"
#include"../constantes/constantes.hpp"
#include <opencv2/core.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/imgproc.hpp>

namespace Vision::plateau{
    /**
     * Liste des différentes bases
    */
    typedef enum{
        untransformed,      // Base originale
        transformedNew,     // Base fraichement calculée
        transformedOld,     // Base de la frame précédante
        transformedBasic,   // Base basique inclue deans le fichier de config
        realSpace           // Coordonnes reels
    }coordSpace;
    
    /**
     * Position d'un tag
    */
    typedef struct{
        vector<cv::Point2f> corners;    // Liste des coins du tag
        cv::Point2f center;             // Centre du tag
        double angle;                   // Orientation du tag
        coordSpace space;               // Espace du tag
    }position;


    /**
     * Tag ArUco
    */
    typedef struct{
        int id;             // ID du tag
        double perimeter;   // Perimètre du tag
        position posPixel;  // Tag avec coordonnes en pixels    
        position posReel;   // Tag avec coordonnes reels
    }arucoTag;

    /**
     * Tag ArUco candidat rejeté
    */
    typedef struct{
        vector<Point2f> corners;    // Liste des coins    
    }arucoTagCandidate;
    
    /**
     * Détecte les tags ArUco de l'image et calcule leurs position et orientation.
     * @param image Image d'entrée
     * @param tags Listes des tags détectés
     * @param camPos Position de la caméra dans l'espace
     * @param useROI Restreint la zone de détection
    */
    void detectAruco(cv::Mat image, vector<arucoTag>& tags, vector<arucoTagCandidate>& bad_tags, coordSpace space, Point3f camPos, bool useROI);

    /**
     * Affiche la liste des tags détectés dans la console, avec posibilité d'afficher des infos supplémentaires
     * @param tags Liste des tags à afficher.
     * @param verbose Affiche également la positions des coins et des centres
    */
    void listAruco(vector<arucoTag>& tags, bool verbose);

    /**
     * Détermine si la table de jeu est détectée (tags 20,21,22,23 présents)
     * @param tags Liste des tags.
     * @return Vrai si table détectée, faux sinon
    */
    bool isTableAruco(vector<arucoTag>& tags);

    /**
     * TODO
    */
    void adjustTagForHeigth(Point3f camPos, arucoTag *tag, double heigth);

    /**
     * TODO
    */
    double tagPerimeter(arucoTag tag);
    
    /**
     * Calcule la matrice de transformation qui permet de passer de l'image non transformée à une image analogue
     * à un repère orthonormé.
     * @param tags Liste des tags
     * @return Matrice de transformation qui permet de transformer une image ou des points 
    */
    Mat calculatePerspective(vector<arucoTag> tags, int width, int height);

    /**
     * Permet de changer les tags repère, de l'espace transformée vers l'espace non-transformé, avec une
     * matrice de passage de l'espace non-transformé verrs l'espace transformé.
     * @param tags Liste des tags
     * @param transform Matrice de transformation
     * @return Vecteur contenant les tags inversés
    */
    vector<arucoTag> inverseAruco(vector<arucoTag> tags, Mat transform);

    /**
     * Quantifie la variation de perspecive d'une frame à l'autre grâce au matrice de transformation de perspective.
     * @param Première matrice de transformation de perspective
     * @param Deuxième matrice de transformation de perspective
     * @return Déviation de perspective d'une frame à l'autre
    */
    float perspectiveDeviation(Mat transform1, Mat transform2, int imgHeight, int imgWidth);

    /**
<<<<<<< HEAD
     * TODO
    */
    Point2f reel2pixel(Point2f pos, int imgWidth, int imgHeight);

    /**
     * TODO
=======
     * [FONCTIONNE PAS] Passe la base des tags à partire de deux matrices de transformation de perspective.
     * @param tags Liste des tags
     * @param transform1 Première matrice de transformation de la perspective
     * @param transform2 Deuxième matrice de transformation de la perspective
>>>>>>> parent of 311215c (Debug & zones circulaires)
    */
    void cameraPosition(vector<arucoTag> tags, Mat cameraMatrix, Point3f *ptr_camPos);

    /**
     * TODO
    */
    Point2f space2reel(Point3f spacePos, Point3f camPos);

    /**
     * TODO
    */
    Rect calculateROI(Point3f camPos, int imgWidth, int imgHeight);


    namespace draw{
        /**
         * Dessine un polygone à partir de la liste des coins.
         * @param image Image sur laquelle dessiner
         * @param corners Coins du polygone
         * @param color Couleur du polygone
        */
        void quad(Mat image, vector<Point2f> corners, Scalar color);
        /**
         * Dessine un cercle à partir d'un centre et d'un rayon
         * @param image Image sur laquelle dessiner
         * @param center Centre du cercle
         * @param radius Rayon du cercle
         * @param color Couleur du cercle
        */
        void circle(Mat image, Point2f center, float radius, Scalar color);

        /**
         * Dessine un tag sur l'image (ID, position, orientation, centre)
         * @param image Image sur laquelle dessiner
         * @param tag Tag à dessiner 
        */
        void tag(Mat image, arucoTag tag);

    }
}



#endif