#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>
#include <iomanip>
#include <sstream>
#include "detectionWork.hpp"

namespace Vision::plateau{

    void detectAruco(cv::Mat image, vector<arucoTag>& tags, vector<arucoTagCandidate>& bad_tags, coordSpace space, Point3f camPos, bool useROI){ 
        cv::aruco::DetectorParameters detectorParams = cv::aruco::DetectorParameters();
        // Paramètres de détection des coins
        detectorParams.polygonalApproxAccuracyRate = 0.10;
        detectorParams.cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
        detectorParams.perspectiveRemovePixelPerCell = 16;
        detectorParams.perspectiveRemoveIgnoredMarginPerCell = 0.25;
        detectorParams.useAruco3Detection = true;
        
        cv::aruco::Dictionary dico = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_4X4_100);
        cv::aruco::ArucoDetector detector(dico, detectorParams);
        vector<int> ids;
        vector<vector<cv::Point2f>> corners, rejected;    
        
        Rect ROI_plateau;   // Zone d'intêret
        Point2f correctionROI;  // Décalage induit par la zone d'interêt
        Point2f center = Point2f(image.cols/2.0, image.rows/2.0);   //Centre de l'image (en pixel)
        if(useROI){
            ROI_plateau = calculateROI(camPos, image.cols, image.rows);
        } 
        else{
            ROI_plateau = Rect(Point2f(0,0), Point2f(image.cols, image.rows));
        }     

        detector.detectMarkers(image(ROI_plateau), corners, ids, rejected); // Déteter les marqueurs
        correctionROI = ROI_plateau.tl(); // Différence entre coin de l'image et coin ROI
        
        Point2f a20, a21, a22, a23, t0, t1, t2, t3, t_center;
        vector<Point2f> screenSpacePoints, tableSpacePoints;
        // Positions des tags de la tables
        t0 = Point2f(CONSTANTES::tag_20_x, CONSTANTES::tag_20_y); // x:750    y:1500
        t1 = Point2f(CONSTANTES::tag_21_x, CONSTANTES::tag_21_y); // x:2250   y:1500
        t2 = Point2f(CONSTANTES::tag_22_x, CONSTANTES::tag_22_y); // x:750    y:500
        t3 = Point2f(CONSTANTES::tag_23_x, CONSTANTES::tag_23_y); // x:2250   y:500
        t_center = (t0+t1+t2+t3)/4.0;
        double scale = CONSTANTES::image_scale;
        
        for(int i = 0; i<ids.size(); i++){
            arucoTag tag;
            Point2f vect;
            tag.id = ids[i];
            tag.posPixel.space = space;
            tag.posPixel.corners = corners[i];
            for(int j = 0; j<4; j++){   // Corriger décalage induit par ROI
                tag.posPixel.corners[j] += correctionROI;
            }
            tag.posPixel.center = (tag.posPixel.corners[0] + tag.posPixel.corners[1] +tag.posPixel.corners[2] +tag.posPixel.corners[3]) / 4.0; // Approximation du centre
            // Calcule de l'angle
            vect = corners[i][1] - corners[i][0];     
            tag.posPixel.angle = -atan2(vect.y, vect.x)*180.0/M_PI;
            if(tag.posPixel.angle<0) tag.posPixel.angle += 360.0;
            // Position reel des tags
            tag.posReel.corners = tag.posPixel.corners;
            for(int j = 0; j<4; j++){
                tag.posReel.corners[j] = (tag.posPixel.corners[j]-center)/scale + t_center;
                tag.posReel.corners[j].y = CONSTANTES::plateau_hauteur - tag.posReel.corners[j].y;   // Inverse l'axe y (plateau haut de 2000mm)
                tag.posReel.center += tag.posReel.corners[j];
            }
            tag.posReel.center /= 4.0;
            
            // Adjust position based on heigth
            if(     (ids[i] >= CONSTANTES::robot_tag_range_BLEUE[0] && ids[i] <= CONSTANTES::robot_tag_range_BLEUE[1])
                ||  (ids[i] >= CONSTANTES::robot_tag_range_JAUNE[0] && ids[i] <= CONSTANTES::robot_tag_range_JAUNE[1]) ){
                adjustTagForHeigth(camPos, &tag, CONSTANTES::robot_hauteur);
            }

            // Angle reel            
            tag.posReel.angle = tag.posPixel.angle;
            if(tag.posReel.angle > 180.0) tag.posReel.angle -= 360.0;
            tag.posReel.angle *= M_PI/180.0;

            // Perimètre réel
            tag.perimeter = tagPerimeter(tag);

            tags.push_back(tag);
        }  
        
        for(auto reject : rejected){
            arucoTagCandidate tag;        
            tag.corners = reject;
            for(int j = 0; j<4; j++){   // Corriger décalage induit par ROI
                tag.corners[j] += correctionROI;
            }
            bad_tags.push_back(tag);
        }          
    }

    void listAruco(vector<arucoTag>& tags, bool verbose){
        printf("Nombre de tags : %d\n",(int)tags.size());
        if(!verbose){
            printf("Tags :");
            for(auto tag : tags){
                printf("  %d",tag.id);
            }
            printf("\n");
            return;
        }
        for(auto tag : tags){
            printf("Tag %d :\n\tCoins :\n",tag.id);
            for(int i = 0; i<tag.posPixel.corners.size(); i++){
                printf("\t\tCoin P %d : x = %.1f\ty = %.1f\n",i,tag.posPixel.corners[i].x,tag.posPixel.corners[i].y);
            }
            for(int i = 0; i<tag.posReel.corners.size(); i++){
                printf("\t\tCoin R %d : x = %.1f\ty = %.1f\n",i,tag.posReel.corners[i].x,tag.posReel.corners[i].y);
            }
            printf("\tCentre P: x = %.1f\ty = %.1f\n",tag.posPixel.center.x,tag.posPixel.center.y);
            printf("\tCentre R: x = %.1f\ty = %.1f\n",tag.posReel.center.x,tag.posReel.center.y);
            printf("Perimeter : %lf\n",tag.perimeter);            
        }
    }

    bool isTableAruco(vector<arucoTag>& tags){
        if(tags.size() < 4) return false;
        bool a20 = false;
        bool a21 = false;
        bool a22 = false;
        bool a23 = false;
        for(auto tag : tags){
            switch(tag.id){
                case 20:
                    a20 = true;
                    continue;
                case 21:
                    a21 = true;
                    continue;
                case 22:
                    a22 = true;
                    continue;
                case 23:
                    a23 = true;
                    continue;
                default:
                    continue;
            }
        }
        return a20 && a21 && a22 && a23;
    }

    void adjustTagForHeigth(Point3f camPos, arucoTag *tag, double heigth){
        Point2f pCam(camPos.x, camPos.y);
        float hCam = camPos.z;
        
        tag->posReel.center = Point2f(0.0, 0.0);
        for(int i = 0; i<4; i++){
            float dCamTag = norm(tag->posReel.corners[i]-pCam);
            tag->posReel.corners[i] = (tag->posReel.corners[i]-pCam)*(1-(heigth/hCam))+pCam;
            tag->posReel.center += tag->posReel.corners[i];
        }
        tag->posReel.center /= 4.0; 
    }

    double tagPerimeter(arucoTag tag){
        double perimeter = 0.0;
        for(int i = 0; i<4; i++) perimeter += norm(tag.posReel.corners[i] - tag.posReel.corners[(i+1)%4] );
        return perimeter;
    }

    Mat calculatePerspective(vector<arucoTag> tags, int imgWidth, int imgHeight){
        vector<Point2f> inputCorners(4);    // Position des tags détecté
        vector<Point2f> outputCorners;      // Positions voulue des tags à l'image
        Point2f a20, a21, a22, a23, t0, t1, t2, t3, t_center;
        // Positions des tags de la tables
        t0 = Point2f(CONSTANTES::tag_22_x, CONSTANTES::tag_22_y); // x:750    y:1500
        t1 = Point2f(CONSTANTES::tag_23_x, CONSTANTES::tag_23_y); // x:2250   y:1500
        t2 = Point2f(CONSTANTES::tag_20_x, CONSTANTES::tag_20_y); // x:750    y:500
        t3 = Point2f(CONSTANTES::tag_21_x, CONSTANTES::tag_21_y); // x:2250   y:500
        t_center = (t0+t1+t2+t3)/4.0;

        double scale = CONSTANTES::image_scale;

        Point2f center = Point2f(imgWidth/2.0, imgHeight/2.0);   // Centre de l'image (en pixel)
        a20 = center + (t0-t_center)*scale;
        a21 = center + (t1-t_center)*scale;
        a22 = center + (t2-t_center)*scale;
        a23 = center + (t3-t_center)*scale;
        
        // Range les coins de sortie dans un vecteur
        outputCorners.push_back(a20);
        outputCorners.push_back(a21);
        outputCorners.push_back(a22);
        outputCorners.push_back(a23);

        // Range les coins d'entrée dans un vecteur (dans le même ordre !!!)
        for(auto tag : tags){
            switch (tag.id){
                case 20:
                    inputCorners[0] = tag.posPixel.center;
                    continue;
                case 21:
                    inputCorners[1] = tag.posPixel.center;
                    continue;
                case 22:
                    inputCorners[2] = tag.posPixel.center;
                    continue;
                case 23:
                    inputCorners[3] = tag.posPixel.center;
                    continue;
                default:
                    continue;
            }                        
        }        
        return getPerspectiveTransform(inputCorners, outputCorners);                
    }

    vector<arucoTag> inverseAruco(vector<arucoTag> tags, Mat transform){
        vector<arucoTag> tagsInv;
        // Réalise la transformée inverse de -transform-
        Mat transformInv;   
        invert(transform, transformInv);
        
        // Recopie les tags et recalcule les champs nécessaires
        for(auto tag : tags){
            arucoTag tagInv;
            Point2f vect;
            tagInv.posPixel.angle = tag.posPixel.angle;            
            tagInv.perimeter = tag.perimeter;
            tagInv.id = tag.id;            
            perspectiveTransform(tag.posPixel.corners, tagInv.posPixel.corners, transformInv);
            
            vect = tagInv.posPixel.corners[1] - tagInv.posPixel.corners[0];            
            tag.posPixel.angle = -atan2(vect.y, vect.x)*180.0/M_PI;
            
            tagInv.posPixel.center = (tagInv.posPixel.corners[0]+tagInv.posPixel.corners[1]+tagInv.posPixel.corners[2]+tagInv.posPixel.corners[3])/4.0;  // Approximation

            tagsInv.push_back(tagInv);
        }
        return tagsInv;
    }

    float perspectiveDeviation(Mat transform1, Mat transform2, int imgHeight, int imgWidth){
        vector<Point2f> ptGrid1, ptGrid2, ptGridBase;
        double diff = 0.0;
        Point2f center = Point2f(imgWidth/2.0, imgHeight/2.0);  // Center of image
        double gridScale = 0.8;                                  // Scale of grid
        int gridSize = 5;                                       // Grid size
        Point2f base = center*(1.0-gridScale);                  // First grid point
        float xStep = (imgHeight*0.8)/(float)(gridSize-1);      // Grid step
        float yStep = (imgWidth *0.8)/(float)(gridSize-1);      // Grid step       
        
        // Generate gridScale^2 points in a grid 
        for(int i = 0; i<gridSize; i++){
            for(int j = 0; j<gridSize; j++){
                ptGridBase.push_back(base+Point2f(xStep*i, yStep*j));
            }
        }
        
        // Transforme les points selon les deux transformées
        perspectiveTransform(ptGridBase, ptGrid1, transform1);
        perspectiveTransform(ptGridBase, ptGrid2, transform2);
        
        // Calcule la distance entre chaque paire de points
        for(int i = 0; i<ptGrid1.size(); i++){
            double calcDiff = sqrt( pow(ptGrid1[i].x-ptGrid2[i].x, 2) + pow(ptGrid1[i].y-ptGrid2[i].y, 2) );
            diff += calcDiff;
        }
        // Retourne la distance moyenne entre les points
        return diff/ptGrid1.size();
    }

    Point2f reel2pixel(Point2f pos, int imgWidth, int imgHeight){
        Point2f pPixel = pos;
        Point2f t_center = Point2f(CONSTANTES::plateau_centre_x, CONSTANTES::plateau_centre_y);   // Centre du terrain
        float scale = CONSTANTES::image_scale;              // Facteur d'echelle
        Point2f center = Point2f(imgWidth/2, imgHeight/2);  // Centre de l'image (en pixel)
        pPixel.y = CONSTANTES::plateau_hauteur-pPixel.y;    // Inverse l'axe y
        pPixel = center + (pPixel-t_center)*scale;          // Passe du repere terrain au repere image
        return pPixel;
    }

    void cameraPosition(vector<arucoTag> tags, Mat cameraMatrix, Point3f *ptr_camPos){
        // Impossible si pas de table
        if(isTableAruco(tags)){
            vector<Point2f> imagePoints(4); // Position sur l'image des points
            vector<Point3f> objectPoints;   // Position dans l'espace des points
            Mat rvec, tvec, rt, r, pos, distCoeffs;
            Point3f camPos;
            // Construire un vecteur des points dans l'espace
            objectPoints.push_back(Point3f(CONSTANTES::tag_20_x,CONSTANTES::tag_20_y,0));
            objectPoints.push_back(Point3f(CONSTANTES::tag_21_x,CONSTANTES::tag_21_y,0));
            objectPoints.push_back(Point3f(CONSTANTES::tag_22_x,CONSTANTES::tag_22_y,0));
            objectPoints.push_back(Point3f(CONSTANTES::tag_23_x,CONSTANTES::tag_23_y,0));
            // Construire un vecteur des points de l'image
            for(auto tag : tags){
                switch (tag.id){
                    case 20:
                        imagePoints[0] = tag.posPixel.center;
                        continue;
                    case 21:
                        imagePoints[1] = tag.posPixel.center;
                        continue;
                    case 22:
                        imagePoints[2] = tag.posPixel.center;
                        continue;
                    case 23:
                        imagePoints[3] = tag.posPixel.center;
                        continue;
                    default:
                        continue;
                }                        
            }
            // Calcule des matrices de transformation et de rotation de la caméra 
            solvePnP(objectPoints, imagePoints, cameraMatrix, distCoeffs, rvec, tvec, false, SOLVEPNP_P3P);
            // Convertion en position
            Rodrigues(rvec, rt);
            transpose(rt, r);
            pos = -r*tvec;
            camPos = Point3f(pos.at<double>(0,0),pos.at<double>(0,1),pos.at<double>(0,2));
            // MàJ de la position de la caméra
            *ptr_camPos = camPos;
        }
        else{
            cout << "Pas de table pour identifier la position de la caméra !" << endl;            
        }
    }

    Point2f space2reel(Point3f spacePos, Point3f camPos){
        Point2f pCam(camPos.x, camPos.y);
        Point2f pSpace(spacePos.x, spacePos.y);
        float hCam = camPos.z;
        float hSpace  = spacePos.z;
        return (pSpace-pCam)*(hCam/(hCam-hSpace))+pCam;
    }

    Rect calculateROI(Point3f camPos, int width, int height){
        return Rect(   
            reel2pixel(space2reel(Point3f(0,0,CONSTANTES::robot_hauteur), camPos), width, height), 
            reel2pixel(space2reel(Point3f(CONSTANTES::plateau_largeur,CONSTANTES::plateau_hauteur,CONSTANTES::robot_hauteur), camPos), width, height)
        );
    }

    namespace draw{

        void quad(Mat image, vector<Point2f> corners, Scalar color){
            for(int i = 0; i<corners.size(); i++){
                line(image, corners[i], corners[ (i+1)%(corners.size()) ], color, 8, LINE_AA);
            }
        }

        void circle(Mat image, Point2f center, float radius, Scalar color){
            circle(image, center, radius, color, 8, LINE_AA);
        }

        void tag(Mat image, arucoTag tag){
            // Dessiner du tag
            quad(image, tag.posPixel.corners, Scalar(255,50,255));
            // Chercher les points min et max qui definisent la bounding box du tag
            Point2f min = Point2f(9999,9999);
            Point2f max = Point2f(0,0);
            for(auto corner : tag.posPixel.corners){
                if(corner.x < min.x) min.x = corner.x;
                if(corner.x > max.x) max.x = corner.x;
                if(corner.y < min.y) min.y = corner.y;
                if(corner.y > max.y) max.y = corner.y;
            }
            // Dessiner le numéro du tag
            putText(image, "id : "+to_string(tag.id), max+Point2f(15, 0), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
            // Position du tag
            putText(image, "x : "+to_string(tag.posReel.center.x), max+Point2f(15, -60), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
            putText(image, "y : "+to_string(tag.posReel.center.y), max+Point2f(15, -30), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
            // Convertire l'angle en string avec 3 décimale de précision et le dessiner
            stringstream stream;
            stream  << fixed << setprecision(3) << tag.posReel.angle;
            string s = stream.str();          
            putText(image, "angle : "+s, max+Point2f(15, -90), FONT_HERSHEY_SIMPLEX, 1, Scalar(0,0,0),3, LINE_AA);
            // Dessiner le centre et le coin 0 du tag
            circle(image, tag.posPixel.corners[0], 10, Scalar(20,20,255), -1, LINE_AA);
            circle(image, tag.posPixel.center, 10, Scalar(255,20,20), -1, LINE_AA);
            // Dessiner une flèche qui part du centre et orientée avec le tag
            arrowedLine(image, tag.posPixel.center, tag.posPixel.center+75*Point2f(cos(tag.posPixel.angle/180.0*M_PI),-sin(tag.posPixel.angle/180.0*M_PI)), Scalar(20,255,20), 6, LINE_AA);
        }
    }
}
