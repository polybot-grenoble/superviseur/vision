#ifndef __VISION__PLATEAU_PLANTES__ 
#define __VISION__PLATEAU_PLANTES__ 

#include"../constantes/constantes.hpp"
#include "../plateau/Zone.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

namespace Vision::plateau{
    void terrainSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams *params);

    void jardiSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams *params);

    void omniSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, PlantsDetectionParams *params);

    double plantAverageSurface(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, Zone_Type zoneType);
}

#endif