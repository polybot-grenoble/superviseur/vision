#ifndef __VISION__PLATEAU_PLATEAUSTATE__ 
#define __VISION__PLATEAU_PLATEAUSTATE__

#include "../io/calibration.hpp"
#include "plantes.hpp"

namespace Vision::plateau{
    enum PlateauDetectionState{
        PLATEAU_NONE = 0,
        PLATEAU_UP_TO_DATE = 1,
        PLATEAU_OUT_OF_DATE = 2
    };

    enum PlantsCalibrationState{
        PLANTS_UNCALIBRATED = 0,
        PLANTS_CALIBRATED = 1
    };

    enum RobotState{
        ROBOT_UNKNOWN,
        ROBOT_UP_TO_DATE,
        ROBOT_OUT_OF_DATE
    };

    struct TableTags{
        bool t20 = false;
        bool t21 = false;
        bool t22 = false;
        bool t23 = false;
    };
    struct PlateauStates{
        TableTags table_tags;
        RobotState robot_bleue;
        RobotState robot_jaune;
        PlantsCalibrationState plants_state;
        PlateauDetectionState plateau_state;
        double plateau_deviation;
    };
    
    struct Robot{
        Point2f position = Point2f(0.0, 0.0);
        double orientation = 0.0;
        int id = -1;
        RobotState robot_state = ROBOT_UNKNOWN; // State of the robot detection
    };

    class PlateauFrame{
        public:
            const int frame_index;      // Index of the current frame
            const double perspective_deviation; // Deviation of the perspective transform matrice between last frame
            const vector<arucoTag> tags;    // Vector of all detected aruco tags
            vector<Zone*> zones;        // Vector of all the plant zones pointers
             
            vector<arucoTag> tagsFiltered;  // Filtered tags
            array<double, CONSTANTES::plant_zone_number> plants_terrain;    // Number of plants in terrain plant zones
            array<double, CONSTANTES::jardi_zone_number> plants_jardi;      // Number of plants in jardi plant zones
            const Point3f camPos;   // Current position of the camera
            const PlateauDetectionState plateauState; // State of plateau detection 
        
            PlateauFrame(int frame, vector<arucoTag> tags, vector<Zone*> zones, Point3f camPos, PlateauDetectionState plateauState, double perspective_deviation);         
    };

    class PlateauAnalyser{
        private:
            vector<PlateauFrame> plateaux;  // Plateaux history
            io::EQUIPE equipe;  // Our team
            Robot robot_bleue;  // The blue robot
            Robot robot_jaune;  // The yellow robot
                       
            Point3f camPosBase; // Basic camera position, obtain from constructor
            Point3f camPosAvg;  // Computed average camera position

            Mat *imageClean;    // Reference to the mat used as Clean
            Mat *imageRender;   // Reference to the mat used as Render

            PlantsDetectionParams plantsParams; // Plant detection parameters
            PlantsCalibrationState plantsState; // State of plant calibration
            
            virtual bool isReadyToCalibrate();
            virtual void calibratePlants();
            virtual void computeAvgCamPos(int size);
            virtual void analyseEquipe();
            virtual void analyseTags();
            virtual void analyseRobot(io::EQUIPE equipe);
            virtual void analysePlantsTerrain();
            virtual void analysePlantsJardi();
            virtual void analyse();
            virtual TableTags findTableTags();

        public:
            PlateauAnalyser(Point3f baseCamPos, io::EQUIPE equipe, PlantsDetectionParams plantsParams, Mat *imageClean, Mat *imageRender);
            virtual void addPlateau(PlateauFrame plateau);
            virtual Point3f getCamPosAvg();
            virtual Point3f getCamPos();
            virtual PlateauStates getPlateauStates();
            virtual void exportAnalysis(double *robot_pos_x, double *robot_pos_y, double *robot_orientation, int plant_number[], int size_of_plant_number); 
            virtual void print();            
    };



}


#endif