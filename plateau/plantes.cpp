#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>
#include "plantes.hpp"

namespace Vision::plateau{
    void terrainSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams *params){
        vector<plateau::Zone*> zonePlantes;
        for(auto zone : zones){
            if(zone->getType() == ZONE_PLANTE_TERRAIN){
                zonePlantes.push_back(zone);
            }
        }
        double planteNumAvg = 0.0;
        for(auto zone : zonePlantes){
            planteNumAvg += zone->plantNumber(imageIn, imageOut, camPos, *params);
        }
        planteNumAvg /= zonePlantes.size();
        cout << "Average number of plantes (pass 1): " << planteNumAvg << endl;
        cout << "Old coeffA : " << params->coeffA << endl;
        params->coeffA *= zonePlantes.size()/planteNumAvg;
        cout << "New coeffA : " << params->coeffA << endl;
        
        planteNumAvg = 0.0;
        for(auto zone : zonePlantes){
            planteNumAvg += zone->plantNumber(imageIn, imageOut, camPos, *params);
        }
        planteNumAvg /= zonePlantes.size();
        cout << "Average number of plantes (pass 2): " << planteNumAvg << endl;
    }

    void jardiSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams *params){
        vector<plateau::Zone*> zonePlantes;
        DetectionParams localParams = *params;
        // Load zones of type ZONE_PLANTE_JARDI in a vector
        for(auto zone : zones){
            if(zone->getType() == ZONE_PLANTE_JARDI){
                zonePlantes.push_back(zone);
            }
        }
        double scan_ratio = 1.0 - params->scan_depth; 
        int scanLimit = localParams.hue[1] - ((localParams.hue[1]-localParams.hue[0]) * scan_ratio);   // How far to go up the hue range
        int initialHueMin = localParams.hue[0];
        double jardiSurface = CONSTANTES::jardi_width * CONSTANTES::jardi_length;   // Surface of a jardinière [mm^2]
        double threshold = jardiSurface * params->scan_threshold;   // Thresold of plants [% of surface]
        double scaleFactor = (CONSTANTES::image_scale * CONSTANTES::image_scale);   // Scale factor to convert pixels to mm^2
        double p2Threshold = 1.65;
        cout << "Scan limit : " << scanLimit << endl;
        cout << "Scan thresold : " << threshold << endl;

        double planteSurfaceAvg = 0.0;
        double alpha = 0.0;
        double beta = 0.0;
        double gamma = 0.0;

        do{
            planteSurfaceAvg = 0.0;
            for(auto zone : zonePlantes){
                planteSurfaceAvg += zone->plantSurface(imageIn, imageOut, camPos, localParams);
            }
            // Detected plant surface averaged per jardinière in mm^2
            planteSurfaceAvg /= zonePlantes.size();
            planteSurfaceAvg /= scaleFactor;
            alpha = (localParams.hue[0]-initialHueMin)/(scanLimit-initialHueMin);
            beta = planteSurfaceAvg/threshold;
            gamma = ( (pow(alpha+1, 2.51)-1)/2.0 ) / beta;
            
            cout << "\tPlant Surface : " << planteSurfaceAvg << " @ [" << localParams.hue[0] << ";" << localParams.hue[1] << "]" << endl;
            cout << "\t\tAlpha : " << alpha << "\tBeta : " << beta << "\tGamma : " << gamma << endl; 
            if(planteSurfaceAvg > threshold){
                if(localParams.hue[0] < scanLimit){
                    localParams.hue[0]++;
                }
                else{
                    cout << "Safety margin reached!" << endl;
                    break;
                }
            } 
        }while(planteSurfaceAvg > threshold);
        if(gamma < p2Threshold){
            cout << "Phase 2" << endl;
            do{
                planteSurfaceAvg = 0.0;
                for(auto zone : zonePlantes){
                    planteSurfaceAvg += zone->plantSurface(imageIn, imageOut, camPos, localParams);
                }
                // Detected plant surface averaged per jardinière in mm^2
                planteSurfaceAvg /= zonePlantes.size();
                planteSurfaceAvg /= scaleFactor;
                alpha = (localParams.hue[0]-initialHueMin)/(scanLimit-initialHueMin);
                beta = planteSurfaceAvg/threshold;
                gamma = ( (pow(alpha+1, 2.51)-1)/2.0 ) / beta;
                
                cout << "\tPlant Surface : " << planteSurfaceAvg << " @ [" << localParams.hue[0] << ";" << localParams.hue[1] << "]" << endl;
                cout << "\t\tAlpha : " << alpha << "\tBeta : " << beta << "\tGamma : " << gamma << endl; 
                if(gamma < p2Threshold){
                    if(localParams.hue[0] < scanLimit){
                        localParams.hue[0]++;
                    }
                    else{
                        cout << "Margin reached!" << endl;
                        break;
                    }
                } 
            }while(gamma < p2Threshold);
        }
        else{
            cout << "Skipping Phase 2" << endl;
        }
        cout << "Final values : " << endl;
        cout << "\tPlant Surface : " << planteSurfaceAvg << " @ [" << localParams.hue[0] << ";" << localParams.hue[1] << "]" << endl;
        params->hue[0] = localParams.hue[0];
        params->hue[1] = localParams.hue[1];
    }

    void omniSetup(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, PlantsDetectionParams *params){
        PlantsDetectionParams inputParams = *params;
        double plantCoeffA, plantCoeffB;
        double startSurface = plantAverageSurface(zones, imageIn, imageOut, camPos, inputParams.terrain, ZONE_PLANTE_TERRAIN); 
        terrainSetup(zones, imageIn, imageOut, camPos, &inputParams.terrain);
        inputParams.jardi.coeffA = inputParams.terrain.coeffA;
        double beforeSurface = plantAverageSurface(zones, imageIn, imageOut, camPos, inputParams.terrain, ZONE_PLANTE_TERRAIN);
        jardiSetup(zones, imageIn, imageOut, camPos, &inputParams.jardi);
        Scalar hueJardi = inputParams.jardi.hue;
        double afterSurface = plantAverageSurface(zones, imageIn, imageOut, camPos, inputParams.jardi, ZONE_PLANTE_TERRAIN);
        cout << "Start surface : " << startSurface << "\tBefore Jardi Surface : " << beforeSurface << "\tAfter Jardi surface : " << afterSurface << endl;
        cout << "Jardi surface pre. : " << plantAverageSurface(zones, imageIn, imageOut, camPos, inputParams.jardi, ZONE_PLANTE_JARDI) << endl;
        cout << "Final coeffs :"
            << "\nTerrain coeffA : " << inputParams.terrain.coeffA
            << "\nTerrain Hue : " << inputParams.terrain.hue
            << "\nTerrain Sat : " << inputParams.terrain.sat 
            << "\nTerrain Val : " << inputParams.terrain.val
            << "\nJardi coeffA : " << inputParams.jardi.coeffA
            << "\nJardi Hue : " << inputParams.jardi.hue
            << "\nJardi Sat : " << inputParams.jardi.sat 
            << "\nJardi Val : " << inputParams.jardi.val << endl;
        inputParams.jardi.coeffA *= beforeSurface/afterSurface;
        cout << "Jardi surface post. : " << plantAverageSurface(zones, imageIn, imageOut, camPos, inputParams.jardi, ZONE_PLANTE_JARDI) << endl;
        cout << "Final coeffs :"
            << "\nTerrain coeffA : " << inputParams.terrain.coeffA
            << "\nTerrain Hue : " << inputParams.terrain.hue
            << "\nTerrain Sat : " << inputParams.terrain.sat 
            << "\nTerrain Val : " << inputParams.terrain.val
            << "\nJardi coeffA : " << inputParams.jardi.coeffA
            << "\nJardi Hue : " << inputParams.jardi.hue
            << "\nJardi Sat : " << inputParams.jardi.sat 
            << "\nJardi Val : " << inputParams.jardi.val << endl;
        *params = inputParams;
    }

    double plantAverageSurface(const vector<plateau::Zone*> zones, const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, Zone_Type zoneType){
        vector<plateau::Zone*> zonePlantes;
        for(auto zone : zones){
            if(zone->getType() == zoneType){
                zonePlantes.push_back(zone);
            }
        }
        double planteNumAvg = 0.0;
        for(auto zone : zonePlantes){
            planteNumAvg += zone->plantNumber(imageIn, imageOut, camPos, params);
        }
        planteNumAvg /= zonePlantes.size();
        return planteNumAvg;
    }
}
