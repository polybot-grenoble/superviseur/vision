#ifndef __VISION__PLATEAU_ZONE__ 
#define __VISION__PLATEAU_ZONE__ 

#include "../constantes/constantes.hpp"
#include "detectionWork.hpp"
#include <vector>
#include <iostream>

using namespace cv;

namespace Vision::plateau{
    enum Zone_Type{
        ZONE_BASIC,
        ZONE_ROBOT_BLEUE,
        ZONE_ROBOT_JAUNE,
        ZONE_PLANTE_TERRAIN,
        ZONE_PLANTE_JARDI
    };

    typedef struct DetectionParams{
        Scalar hue;
        Scalar sat;
        Scalar val;
        double coeffA;
        double coeffB;
        double scan_depth;
        double scan_threshold;
    }Detection_Params;

    typedef struct PlantsDetectionParams{
        DetectionParams terrain;
        DetectionParams jardi;
    }Plants_Detection_Params;

    class Zone {
        protected:
            Zone_Type type;         // Type de la zone
            double plant_number;    // Number of plant inside the zone
            int id;                 // Zone id, must be unique
            virtual long whitePixelCounter(Mat &zoneImage, Mat &zoneMask, Mat &colorMask, Mat &val, DetectionParams ranges);
        public:
            Zone();
            virtual Zone_Type getType();
            virtual int getID();
            virtual double getPlantNumber();
            virtual void drawZoneTags(Mat image, std::vector<arucoTag> tags);
            virtual std::vector<arucoTag> detectTag(std::vector<arucoTag> tags);
            virtual void calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos);
            virtual double plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display = false );
            virtual double plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params);       
    };

    class ZoneRectangle : public Zone {
        private:
            Point2f minCorner;
            Point2f maxCorner;
        public:
            ZoneRectangle(Point2f minCorner, Point2f maxCorner, Zone_Type type, int id);
            virtual Zone_Type getType();
            virtual void drawZone(Mat image);
            virtual void drawZoneTags(Mat image, std::vector<arucoTag> tags);
            virtual std::vector<arucoTag> detectTag(std::vector<arucoTag> tags);
            virtual void calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos);
            virtual double plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display = false );
            double plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params) override;
    };
    
    class ZoneCercle : public Zone {
        private:
            float radius;
            Point2f center;
        public:
            ZoneCercle(Point2f center, float radius, Zone_Type type, int id);
            virtual Zone_Type getType();
            virtual void drawZone(Mat image);
            virtual void drawZoneTags(Mat image, std::vector<arucoTag> tags);
            virtual std::vector<arucoTag> detectTag(std::vector<arucoTag> tags);
            virtual void calibratePlants(const Mat &imageIn, Mat &imageOut, Point3f camPos);
            virtual double plantSurface(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params, bool display = false );
            double plantNumber(const Mat &imageIn, Mat &imageOut, Point3f camPos, DetectionParams params) override;
    };
}
#endif