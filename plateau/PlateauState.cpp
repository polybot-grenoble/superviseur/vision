#include "PlateauState.hpp"

using namespace std;

namespace Vision::plateau{

    PlateauFrame::PlateauFrame(int frame, vector<arucoTag> tags, vector<Zone*> zones, Point3f camPos, PlateauDetectionState plateauState, double perspective_deviation) : 
    frame_index(frame), tags(tags), camPos(camPos), plateauState(plateauState), perspective_deviation(perspective_deviation)
    {
        for(auto zone : zones){
            this->zones.push_back(zone);
        }
        plants_terrain.fill(-1.0);
        plants_jardi.fill(-1.0);
    }

    PlateauAnalyser::PlateauAnalyser(Point3f baseCamPos, io::EQUIPE equipe, PlantsDetectionParams plantsParams, Mat *imageClean, Mat *imageRender){
        this->camPosBase = baseCamPos;
        this->camPosAvg = baseCamPos;
        this->equipe = equipe;
        this->plantsParams = plantsParams;
        this->imageClean = imageClean;
        this->imageRender = imageRender;
        this->plantsState = PLANTS_UNCALIBRATED;        
    }

    void PlateauAnalyser::addPlateau(PlateauFrame plateau){
        this->plateaux.push_back(plateau);

        analyse();
    }

    bool PlateauAnalyser::isReadyToCalibrate(){
        if(plateaux.size()<3) return false;
        if(plateaux.back().plateauState != PLATEAU_UP_TO_DATE ) return false;
        if(plateaux.rbegin()[1].plateauState != PLATEAU_UP_TO_DATE ) return false;  // Element before last using reverse iterator
        if(plateaux.back().perspective_deviation > CONSTANTES::analyse_deviation_safe_value) return false;
        return true;
    }

    void PlateauAnalyser::calibratePlants(){ 
        omniSetup(plateaux.back().zones, *imageClean, *imageRender, camPosAvg, &plantsParams);
    }

    void PlateauAnalyser::computeAvgCamPos(int size){
        int plateaux_size = plateaux.size();

        // If not enough plateaux, exit to avoid detection setting up problems 
        if(plateaux_size < 3){
            this->camPosAvg = this->camPosBase;
            return;
        }

        Point3f camPosAvg_local = Point3f(0.0, 0.0, 0.0);
        vector<Point3f> foundCamPos;

        // Try to find the last [size] camera position with low deviation and the last to plateaux up to date
        auto it = plateaux.rbegin();
        while( (it != plateaux.rend()-1) && (foundCamPos.size() < size) ){
            if( (it->plateauState == PLATEAU_UP_TO_DATE) && ((it+1)->plateauState == PLATEAU_UP_TO_DATE) && it->perspective_deviation < CONSTANTES::analyse_deviation_safe_value){
                foundCamPos.push_back(it->camPos);
            }
            it++;
        }

        // If enough plateaux for the size of the average :
        if(foundCamPos.size() >= size){
            // Sum last [size] camera positions and divide by [size] for average position
            for(int i = 0; i<size; i++){
                camPosAvg_local += foundCamPos[i];
            }
            camPosAvg_local /= size;            
        }
        // If less plateaux than the size of the average :
        else if(foundCamPos.empty()){
            camPosAvg_local = camPosBase;
        }
        else{
            // Sum all camera positions and divide by number of plateaux for average position 
            for(int i = 0; i<foundCamPos.size(); i++){
                camPosAvg_local += foundCamPos[i];
            }
            camPosAvg_local /= (double)foundCamPos.size();
        }
        // Update field
        this->camPosAvg = camPosAvg_local;
    }

    Point3f PlateauAnalyser::getCamPosAvg(){
        return camPosAvg;
    }

    Point3f PlateauAnalyser::getCamPos(){
        if(plateaux.empty()) return camPosBase;
        return plateaux.back().camPos;
    }

    PlateauStates PlateauAnalyser::getPlateauStates(){
        PlateauStates plateau_states;
        plateau_states.plants_state = this->plantsState;
        plateau_states.plateau_deviation = this->plateaux.back().perspective_deviation;
        plateau_states.plateau_state = this->plateaux.back().plateauState;
        plateau_states.robot_bleue = robot_bleue.robot_state;
        plateau_states.robot_jaune = robot_jaune.robot_state;
        plateau_states.table_tags = findTableTags();
        return plateau_states;
    }
    
    void PlateauAnalyser::analyseEquipe(){
        // Return if plateau not up to date
        if(!isReadyToCalibrate()){
            printf("Plateau isn't up to date, can't determine equipe!\n");
            return;
        }
        // Camera on the left side, equipe is bleue
        if(camPosAvg.x < CONSTANTES::plateau_centre_x - CONSTANTES::analyse_equipe_detection_margin){
            equipe = io::EQUIPE_BLEUE;
        }
        // Else, equipe is jaune
        else if(camPosAvg.x > CONSTANTES::plateau_centre_x + CONSTANTES::analyse_equipe_detection_margin){
            equipe = io::EQUIPE_JAUNE;
        }
    }

    void PlateauAnalyser::analyseTags(){
        bool verbose = true;
        vector<arucoTag> tags = plateaux.back().tags;
        double perimeter_max = CONSTANTES::analyse_tag_perimeter_max;
        double perimeter_min = CONSTANTES::analyse_tag_perimeter_min;
        
        // Skip culling if plateau not yet in place
        if(plateaux.back().plateauState == PLATEAU_NONE){
            if(verbose) printf("Skipped tags culling because plateau not established!\n");
            return;
        }

        // Find and remove tags with absurd perimeters and coordinates
        for(auto tag = tags.begin(); tag != tags.end(); tag++){       
            // Search for absurd perimeters     
            if( (tag->perimeter > perimeter_max) || (tag->perimeter < perimeter_min) ){
                if(verbose) printf("Removed tag %d of perimeter %lf [%lf;%lf]",tag->id,tag->perimeter,perimeter_min,perimeter_max);
                // Erase and avoid iterator invalidation
                tag = tags.erase(tag);
                tag--;
            }
            // Search for absurd coordinates
            if( (tag->posReel.center.x > CONSTANTES::plateau_largeur) || (tag->posReel.center.x < 0.0) || 
                (tag->posReel.center.y > CONSTANTES::plateau_hauteur) || (tag->posReel.center.y < 0.0) ){
                if(verbose) cout << "Removed tag " << tag->id << " of position " << tag->posReel.center << endl;
                // Erase and avoid iterator invalidation
                tag = tags.erase(tag);
                tag--;
            }
        }
        // Insert tags into tagFiltered field
        plateaux.back().tagsFiltered = tags;
    }

    void PlateauAnalyser::analyseRobot(io::EQUIPE equipe_local){
        // Select which robot to find
        Robot *selectedRobot;
        String robotColor;
        int tag_range[2];
        switch(equipe_local){
            case io::EQUIPE_BLEUE:
                selectedRobot = &robot_bleue;
                robotColor = "Blue";
                tag_range[0] = CONSTANTES::robot_tag_range_BLEUE[0];
                tag_range[1] = CONSTANTES::robot_tag_range_BLEUE[1];
                break;
            case io::EQUIPE_JAUNE:
                selectedRobot = &robot_jaune;
                robotColor = "Yellow";
                tag_range[0] = CONSTANTES::robot_tag_range_JAUNE[0];
                tag_range[1] = CONSTANTES::robot_tag_range_JAUNE[1];
                break;
            default:
                cout << "Wtf man..." << endl;
                return;
        }

        // If robots ids unknown
        if(selectedRobot->robot_state == ROBOT_UNKNOWN){
            // Fill robotTagInit with tag ids in range for selected robot
            vector<arucoTag> robotTagsInit;
            for(auto tag : plateaux.back().tagsFiltered){
                if( (tag.id >= tag_range[0]) && 
                    (tag.id <= tag_range[1]) ){
                        robotTagsInit.push_back(tag);
                    }
            }
            // According to the number of tags found :
            switch(robotTagsInit.size()){
                // If 0 do nothing
                case 0:
                    cout << "No " << robotColor << " robot found!" << endl;
                    break;
                // If 1 fill the robot with tag parameters
                case 1:
                    cout << robotColor << " robot found!" << endl;
                    selectedRobot->id = robotTagsInit.back().id;
                    selectedRobot->orientation = robotTagsInit.back().posReel.angle;
                    selectedRobot->position = robotTagsInit.back().posReel.center;
                    selectedRobot->robot_state = ROBOT_UP_TO_DATE;
                    break;
                // If more than 1 :
                default:
                    cout << "Multiple " << robotColor << " robots found!" << endl;
                    // Search for tags inside a zone
                    vector<arucoTag> tagZoneBleue;
                    for(auto zone : plateaux.back().zones){
                        if(zone->getType() == ZONE_ROBOT_BLEUE){
                            vector<arucoTag> found = zone->detectTag(robotTagsInit);
                            tagZoneBleue.insert(tagZoneBleue.end(), found.begin(), found.end());
                        } 
                    }
                    // If empty, default to the last tag found outside of the zones
                    if(tagZoneBleue.empty()){
                        cout << "\tNo blue robot found in starting zones!" << endl;
                        selectedRobot->id = robotTagsInit.back().id;
                        selectedRobot->orientation = robotTagsInit.back().posReel.angle;
                        selectedRobot->position = robotTagsInit.back().posReel.center;
                        selectedRobot->robot_state = ROBOT_UP_TO_DATE;
                    }
                    // If 1 inside, fill the robot with tag parameters
                    else if(tagZoneBleue.size() == 1){
                        cout << "\tOne blue robot found in starting zones!" << endl;
                        selectedRobot->id = tagZoneBleue.back().id;
                        selectedRobot->orientation = tagZoneBleue.back().posReel.angle;
                        selectedRobot->position = tagZoneBleue.back().posReel.center;
                        selectedRobot->robot_state = ROBOT_UP_TO_DATE;               
                    }
                    // If more than one default to the last tag found inside of the zones
                    else{
                        cout << "\tMultiple blue robots found in starting zones!" << endl;
                        selectedRobot->id = tagZoneBleue.back().id;
                        selectedRobot->orientation = tagZoneBleue.back().posReel.angle;
                        selectedRobot->position = tagZoneBleue.back().posReel.center;
                        selectedRobot->robot_state = ROBOT_UP_TO_DATE;
                    }
                    break;
            }
        }
        // If robot ids known
        else{
            // Load robotTags with tags with the same id as the robot field
            vector<arucoTag> robotTags;
            for(auto tag : plateaux.back().tagsFiltered){
                if(tag.id == selectedRobot->id) robotTags.push_back(tag);
            }
            // Update if only one tag is found
            if(robotTags.size() == 1){
                selectedRobot->orientation = robotTags.back().posReel.angle;
                selectedRobot->position = robotTags.back().posReel.center;
                selectedRobot->robot_state = ROBOT_UP_TO_DATE;
            }
            // If none found then not up to date anymore
            else if(robotTags.empty()){
                selectedRobot->robot_state = ROBOT_OUT_OF_DATE;
            }
            // If multiple found, find the closest to last position and update
            else{
                arucoTag closestTag;
                double distToLast = 9999.0;
                for(auto tag : robotTags){
                    if( norm(tag.posReel.center - selectedRobot->position) < distToLast ){
                        distToLast = norm(tag.posReel.center - selectedRobot->position);
                        closestTag = tag;
                    }
                }
                selectedRobot->orientation = closestTag.posReel.angle;
                selectedRobot->position = closestTag.posReel.center;
                selectedRobot->robot_state = ROBOT_UP_TO_DATE;
            } 
        }
    }

    void PlateauAnalyser::analysePlantsTerrain(){
        double plant_number;
        int array_index;

        // For all zones :
        for(auto zone : plateaux.back().zones){
            // If the zone is of the right type
            if(zone->getType() == ZONE_PLANTE_TERRAIN){
                // Get number of plants inside of the zone and compute the array index
                plant_number = zone->plantNumber(*imageClean, *imageRender, camPosAvg, plantsParams.terrain);
                array_index = zone->getID()-CONSTANTES::id_zone_plantes-1;
                // Check index validity, then write the plant number into the array
                if( (array_index < 0) || (array_index>=CONSTANTES::plant_zone_number) ){
                    cout << "Something went wrong with zone [" << zone->getID() << "]!" << endl;
                }
                else{
                    plateaux.back().plants_terrain[array_index] = plant_number;
                }
            }
        }
    }

    void PlateauAnalyser::analysePlantsJardi(){
        double plant_number;
        int array_index;

        // For all zones :
        for(auto zone : plateaux.back().zones){
            // If the zone is of the right type
            if(zone->getType() == ZONE_PLANTE_JARDI){
                // Get number of plants inside of the zone and compute the array index
                plant_number = zone->plantNumber(*imageClean, *imageRender, camPosAvg, plantsParams.jardi);
                array_index = zone->getID()-CONSTANTES::id_zone_jardis-1;
                // Check index validity, then write the plant number into the array
                if( (array_index < 0) || (array_index>=CONSTANTES::jardi_zone_number) ){
                    cout << "Something went wrong with zone [" << zone->getID() << "]!" << endl;
                }
                else{
                    plateaux.back().plants_jardi[array_index] = plant_number;
                }
            }
        }
    }

    void PlateauAnalyser::analyse(){
        // Start by updating the camera average position
        computeAvgCamPos(5);
        // If plants not calibrated and ready to be, calibrate
        if( (this->plantsState == PLANTS_UNCALIBRATED) && isReadyToCalibrate() ){
            calibratePlants();
            this->plantsState = PLANTS_CALIBRATED;
        }
        // If equipe not known, try to detect equipe
        if( this->equipe == io::EQUIPE_UNKNOWN ){
            analyseEquipe();
        }
        // Analyse tags
        analyseTags();
        // If plateau is up to date or out of date (but not not found), analyse robots
        if( (plateaux.back().plateauState == PLATEAU_UP_TO_DATE) || (plateaux.back().plateauState == PLATEAU_OUT_OF_DATE) ){
            analyseRobot(io::EQUIPE_BLEUE);
            analyseRobot(io::EQUIPE_JAUNE);
        }
        // If plants calibrated, analyse plants
        if(this->plantsState == PLANTS_CALIBRATED){
            analysePlantsTerrain();
            analysePlantsJardi();
        }
            
    }

    TableTags PlateauAnalyser::findTableTags(){
        TableTags table_tags;
        for(auto tag : plateaux.back().tagsFiltered){
            switch(tag.id){
                case 20:
                    table_tags.t20 = true;
                    continue;
                case 21:
                    table_tags.t21 = true;
                    continue;
                case 22:
                    table_tags.t22 = true;
                    continue;
                case 23:
                    table_tags.t23 = true;
                    continue;
                default:
                    continue;
            }
        }
        return table_tags;
    }

    void PlateauAnalyser::print(){
        cout << "Equipe : " << equipe << endl;
        cout << "Robot bleue : " << "\n\tPosition : " << robot_bleue.position << "\n\tOrientation : " << robot_bleue.orientation << "\n\tID : " << robot_bleue.id << "\n\tRobot State : " << robot_bleue.robot_state << endl;
        cout << "Robot jaune : " << "\n\tPosition : " << robot_jaune.position << "\n\tOrientation : " << robot_jaune.orientation << "\n\tID : " << robot_jaune.id << "\n\tRobot State : " << robot_jaune.robot_state << endl;
        cout << "Plantes : " << endl;
        cout << "\tTerrain :" << endl;
        for(int i = 0; i<plateaux.back().plants_terrain.size(); i++){
            cout << "\t\tZone " << i << " : " << plateaux.back().plants_terrain[i] << endl;
        }
        cout << "\tJardi :" << endl;
        for(int i = 0; i<plateaux.back().plants_jardi.size(); i++){
            cout << "\t\tZone " << i << " : " << plateaux.back().plants_jardi[i] << endl;
        }
        cout << "Plateau state : " << plateaux.back().plateauState << endl;
        cout << "Plantes state : " << plantsState << endl;
        cout << "Perspective deviation : " << plateaux.back().perspective_deviation << endl;
        cout << "isReadyToCalibrate : " << isReadyToCalibrate() << endl;
        cout << "Average camera position : " << getCamPosAvg() << endl;
        cout << "Last camera position : " << getCamPos() << endl;
    }

    void PlateauAnalyser::exportAnalysis(double *robot_pos_x, double *robot_pos_y, double *robot_orientation, int plant_number[], int size_of_plant_number){
        // If plants not calibrated, fill buffer with zeros
        if(plantsState != PLANTS_CALIBRATED){
            printf("Plants uncalibrated !\n");
            for(int i = 0; i<size_of_plant_number; i++){
                plant_number[i] = 0;
            }
        }
        // If plants calibrated, try to fill buffer with info
        else{
            // If size of stored and provided buffers mismatched, fill buffer with zeros
            if(plateaux.back().plants_terrain.size() + plateaux.back().plants_jardi.size() != size_of_plant_number){
                printf("Input plant buffer size different than output buffer size! \n");
                for(int i = 0; i<size_of_plant_number; i++){
                    plant_number[i] = 0;
                }
            }
            // If Ok, fill with stored plant number rounded to nearest integer
            else{
                for(int i = 0; i<plateaux.back().plants_terrain.size(); i++){
                    plant_number[i] = cvRound(plateaux.back().plants_terrain[i]);
                }
                for(int i = 0; i<plateaux.back().plants_jardi.size(); i++){
                    plant_number[i+plateaux.back().plants_terrain.size()] = cvRound(plateaux.back().plants_jardi[i]);
                }
            }
            
        }
        // If equipe unknown, can't do anything
        if(equipe == io::EQUIPE_UNKNOWN){
            printf("Equipe unknown!\n");
            *robot_pos_x = 0.0;
            *robot_pos_y = 0.0;
            *robot_orientation = 0.0;
        }
        // Else fill accoding to equipe
        else if(equipe == io::EQUIPE_BLEUE){
            if(robot_bleue.robot_state != ROBOT_UP_TO_DATE) printf("Robot is not up to date!\n");
            *robot_pos_x = robot_bleue.position.x;
            *robot_pos_y = robot_bleue.position.y;
            *robot_orientation = robot_bleue.orientation;
        }
        else if(equipe == io::EQUIPE_JAUNE){
            if(robot_jaune.robot_state != ROBOT_UP_TO_DATE) printf("Robot is not up to date!\n");
            *robot_pos_x = robot_jaune.position.x;
            *robot_pos_y = robot_jaune.position.y;
            *robot_orientation = robot_jaune.orientation;
        }
    }
}