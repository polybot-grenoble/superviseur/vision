#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <lccv.hpp>

#include "../plateau/detectionWork.hpp"
#include "../io/camera.hpp"
#include "../plateau/Zone.hpp"
#include "../plateau/plantes.hpp"
#include "../plateau/PlateauState.hpp"
#include "../io/Chrono.hpp"

using namespace Vision;
using namespace cv;


int main (int argc, char** argv) {

    lccv::PiCamera *cam;
    io::calibration Calibration;
    Mat imageClean, imageRender, raw, sortie;
    io::fisheye_maps distortion_map;

    vector<plateau::arucoTag> tags;                 // Contient les tous les tags détectés
    vector<plateau::arucoTagCandidate> bad_tags;    // Contient les tag candidats rejetés
    Mat transform, transformOld;                    // Matrice de transformation de la perspective
    bool foundTransform = false;                    // Matrice de transformation réelle trouvée
    double perspective_deviation = -1.0;            // Calculated perspective deviation
    plateau::PlateauDetectionState plateauState = plateau::PLATEAU_NONE;
    Point3f camPos;                                 // Position de la caméra dans l'espace    

    /* Initialize Chrono object for profiling purpose */
    io::Chrono chronometer = io::Chrono();

    int timeoutCounter = 0;     // Keep track of consecutive timeouts
    int frameIndex = 0;         // Index of the current frame

    char c = 0;

    if (argc < 2) {
        fprintf(stderr, "%s <calibration.json>\n", argv[0]);
        return 1;
    }


    // Charge et affiche les paramètres de la config
    Calibration = io::charge(argv[1]);
    io::affiche(Calibration);

    // Log stdout
    if(Calibration.logStdout == true){
        auto t = std::time(nullptr);
        auto tm = *localtime(&t);
        ostringstream oss;
        oss << put_time(&tm, "%Y-%m-%d-%H%M%S");
        auto str = oss.str();
        string fileName = "logs_";
        fileName.append(str);
        freopen(fileName.c_str(), "w", stdout);
    }

    transform = Calibration.perspective;    // Charge la transformation de perspective basique du fichier de calibration
    transform.copyTo(transformOld);
    camPos = Calibration.position;          // Position basique de la caméra
    
    // Charge les paramètres de détection des plantes
    plateau::PlantsDetectionParams plantesParams;
    io::loadConfigPlantes(Calibration, &plantesParams);

    plateau::PlateauAnalyser Analyser = plateau::PlateauAnalyser(Calibration.position, Calibration.equipe, plantesParams, &imageClean, &imageRender);
    
    distortion_map = io::calculeFisheye(Calibration);

    cam = io::setupCamera(Calibration);
    
    /* Transformation en direct */
    namedWindow("Image", WINDOW_KEEPRATIO);
    
    switch(Calibration.expoMethode){
        case io::AutoExpo::BURNT:
            printf("Début de l'autoexpo par méthode 1 :\n");
            if(io::setupCameraGain(*cam, raw, imageClean, sortie, distortion_map)){
                printf("Echec de l'autoexpo par méthode 1!\n");
            }
            printf("Fin de l'autoexpo par méthode 1 :\n");
            break;
        case io::AutoExpo::GRADIANT:
            printf("Début de l'autoexpo par méthode 2 :\n");
            io::setupCameraGradiant(*cam, raw, imageClean, sortie, distortion_map);
            printf("Fin de l'autoexpo par méthode 2 :\n");
            break;
        case io::AutoExpo::TAGS:
            printf("Début de l'autoexpo par méthode 3 :\n");
            io::setupCameraTags(*cam, raw, imageClean, sortie, distortion_map);
            printf("Fin de l'autoexpo par méthode 3 :\n");
            break;
        case io::AutoExpo::NONE:
            printf("Exposition par defaut\n");
            break;

    }    

    // Creation des zones du plateau
    vector<plateau::Zone*> zonePlateau;
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(0,0),          Point2f(450,450),   plateau::ZONE_ROBOT_BLEUE, 101));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(0,775),        Point2f(450,1225),  plateau::ZONE_ROBOT_JAUNE, 102));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(0,1550),       Point2f(450,2000),  plateau::ZONE_ROBOT_BLEUE, 103));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(2550,0),       Point2f(3000,450),  plateau::ZONE_ROBOT_JAUNE, 104));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(2550,775),     Point2f(3000,1225), plateau::ZONE_ROBOT_BLEUE, 105));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(2550,1550),    Point2f(3000,2000), plateau::ZONE_ROBOT_JAUNE, 106));
    // Central zone
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(1500,1000),   325,    plateau::ZONE_BASIC, 100));
    // Definition en brut des 6 cercles ou se trouvent les plantes 
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(1500,1500),   125,    plateau::ZONE_PLANTE_TERRAIN, 201));
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(2000,1300),   125,    plateau::ZONE_PLANTE_TERRAIN, 202));
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(2000,700),    125,    plateau::ZONE_PLANTE_TERRAIN, 203));
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(1500,500),    125,    plateau::ZONE_PLANTE_TERRAIN, 204));
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(1000,700),    125,    plateau::ZONE_PLANTE_TERRAIN, 205));
    zonePlateau.push_back(new plateau::ZoneCercle(Point2f(1000,1300),   125,    plateau::ZONE_PLANTE_TERRAIN, 206));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(-170,450),     Point2f(-20,775), plateau::ZONE_PLANTE_JARDI, 301));
    zonePlateau.push_back(new plateau::ZoneRectangle(Point2f(-170,1225),    Point2f(-20,1550), plateau::ZONE_PLANTE_JARDI, 302));

    cam->startVideo();    
    // Main loop
    while (c != 27) { // Echap
        chronometer.startChrono();
        if (!cam->getVideoFrame(raw, 200)) {
            fprintf(stderr, "ERR/Capture/Timeout\n");
            fflush(stderr);
            timeoutCounter++;
            if(timeoutCounter > 30){
                printf("Unusual timeout detected !\nRestarting camera...\n");
                printf("Attempting to recreate camera...\n");
                cam = io::resetCamera(cam);
                printf("Camera reset!\n");
                timeoutCounter = 0;
            }
        } 
        else {
            printf("~~~~~~~~~~~~~~~~~~~~~    START OF FRAME    ~~~~~~~~~~~~~~~~~~~~~\n");
            chronometer.inChrono("Fisheye");
            io::appliqueFisheye(raw, imageClean, distortion_map);    
            chronometer.outChrono("Fisheye");

            tags.clear();
            bad_tags.clear();

            if(c == 's'){
                Calibration.filter_sharp_do = !Calibration.filter_sharp_do;
            }
            if(c == 'c'){
                Calibration.filter_CLAHE_do = !Calibration.filter_CLAHE_do;
            }
            
            if(Calibration.filter_sharp_do){
                chronometer.inChrono("Sharpening");
                io::filterSharp(imageClean, Calibration.filter_sharp_strength);
                chronometer.outChrono("Sharpening");
            }            

            if(Calibration.filter_CLAHE_do){
                chronometer.inChrono("CLAHE");
                io::filterCLAHE(imageClean, Calibration.filter_CLAHE_clipLimit, Calibration.filter_CLAHE_gridSize);
                chronometer.outChrono("CLAHE");
            }

            // Detection routine
            chronometer.inChrono("Tag detection");
            perspective_deviation = plateau::perspectiveDeviation(transform, transformOld, imageClean.rows, imageClean.cols);
            std::cout << "Deviation : " <<  perspective_deviation << endl;
            if(!foundTransform){ 
                printf("Pas de transformée récente trouvée\n");
                plateau::detectAruco(imageClean, tags, bad_tags, plateau::coordSpace::untransformed, Analyser.getCamPosAvg(), false);
                if(plateau::isTableAruco(tags)){  
                    printf("\tTable trouvée\n");               
                    transform = plateau::calculatePerspective(tags, imageClean.cols, imageClean.rows);
                    foundTransform = true;
                    plateauState = plateau::PLATEAU_UP_TO_DATE;
                    warpPerspective(imageClean, imageClean, transform, Size(imageClean.cols, imageClean.rows));
                    tags.clear();
                    plateau::detectAruco(imageClean, tags, bad_tags, plateau::coordSpace::transformedNew, Analyser.getCamPosAvg(), false); //true
                    plateau::cameraPosition(plateau::inverseAruco(tags, transform), Calibration.camera, &camPos);
                    printf("\tTransformée calculée\n");                    
                }
                else{
                    printf("\tPas de table trouvée\n");
                    warpPerspective(imageClean, imageClean, transform, Size(imageClean.cols, imageClean.rows));
                    printf("\tPerspective calculée avec transformée basique\n");
                    tags.clear();
                    plateau::detectAruco(imageClean, tags, bad_tags, plateau::coordSpace::transformedBasic, Analyser.getCamPosAvg(), false);
                    if(plateau::isTableAruco(tags)){
                        printf("\t\tTable trouvée\n");
                        transform = plateau::calculatePerspective(tags, imageClean.cols, imageClean.rows);
                        foundTransform = true;
                        plateauState = plateau::PLATEAU_UP_TO_DATE;
                        warpPerspective(imageClean, imageClean, transform, Size(imageClean.cols, imageClean.rows));
                        tags.clear();
                        plateau::detectAruco(imageClean, tags, bad_tags, plateau::coordSpace::transformedNew, Analyser.getCamPosAvg(), false); //true
                        plateau::cameraPosition(plateau::inverseAruco(tags, transform), Calibration.camera, &camPos);
                        printf("\t\tTransformée calculée\n");
                    } 
                }
            }
            else{
                printf("Transformée récente trouvée\n");
                warpPerspective(imageClean, imageClean, transform, Size(imageClean.cols,imageClean.rows));
                plateau::detectAruco(imageClean, tags, bad_tags, plateau::coordSpace::transformedOld, Analyser.getCamPosAvg(), false); //true
                if(plateau::isTableAruco(tags)){
                        plateauState = plateau::PLATEAU_UP_TO_DATE;
                        transform.copyTo(transformOld);
                        transform = plateau::calculatePerspective(plateau::inverseAruco(tags, transform), imageClean.cols, imageClean.rows);
                        plateau::cameraPosition(plateau::inverseAruco(tags, transform), Calibration.camera, &camPos);              
                        printf("\tTransformée recalculée\n");                                  
                }
                else{                    
                    plateauState = plateau::PLATEAU_OUT_OF_DATE;
                    printf("\tPas de table trouvée\n");
                }
            }
            // Display plateau state
            switch(plateauState){
                case plateau::PLATEAU_NONE:
                    printf("Plateau detection state : NONE\n");
                    break;
                case plateau::PLATEAU_OUT_OF_DATE:
                    printf("Plateau detection state : OUT OF DATE\n");
                    break;
                case plateau::PLATEAU_UP_TO_DATE:
                    printf("Plateau detection state : UP TO DATE\n");
                    break;
            }
            chronometer.outChrono("Tag detection");
         
            imageClean.copyTo(imageRender);                 

            // // Print les tags détectés et les plantes
            // for(int i = 0; i<zonePlateau.size(); i++){
            //     if( (zonePlateau[i]->getType() == plateau::ZONE_BASIC) ||
            //         (zonePlateau[i]->getType() == plateau::ZONE_ROBOT_BLEUE) ||
            //         (zonePlateau[i]->getType() == plateau::ZONE_ROBOT_JAUNE)){
            //         std::vector<plateau::arucoTag> tagInside;
            //         tagInside = zonePlateau[i]->detectTag(tags);
            //         printf("Tag(s) dans la zone %d : ",zonePlateau[i]->getID());
                    
            //         for(int j = 0; j<tagInside.size(); j++){
            //             printf("%d  ",tagInside[j].id);
            //         }
            //         printf("\n");
            //     }                
            //     else if(zonePlateau[i]->getType() == plateau::ZONE_PLANTE_TERRAIN){
            //         double plantNumber;
            //         plantNumber = zonePlateau[i]->plantNumber(imageClean, imageRender, Analyser.getCamPosAvg(), plantesParams.terrain);
            //         printf("Plantes dans la zone %d : %lf\n",zonePlateau[i]->getID(),plantNumber);
            //     }
            //     else if(zonePlateau[i]->getType() == plateau::ZONE_PLANTE_JARDI){
            //         double plantNumber;
            //         plantNumber = zonePlateau[i]->plantNumber(imageClean, imageRender, Analyser.getCamPosAvg(), plantesParams.jardi);
            //         printf("Plantes dans la jardiniere %d : %lf\n",zonePlateau[i]->getID(),plantNumber);
            //     }
                
            // }

            // Plateau analysis
            plateau::PlateauFrame currentPlateau(frameIndex, tags, zonePlateau, camPos, plateauState, perspective_deviation);
            Analyser.addPlateau(currentPlateau);
            Analyser.print();

            // Fill buffers with analysis results
            double robot_pos_x;
            double robot_pos_y;
            double robot_orientaion;
            const int zone_plant_number = 12;
            int plant_number[zone_plant_number];
            Analyser.exportAnalysis(&robot_pos_x, &robot_pos_y, &robot_orientaion, plant_number, zone_plant_number);

            // // Setup zones
            // if(chronometer.getFrameNum() == 2){
            //     plateau::omniSetup(zonePlateau, imageClean, imageRender, Analyser.getCamPosAvg(), &plantesParams);
            // }

            // Dessine les zones
            for(auto zone : zonePlateau){
                zone->drawZoneTags(imageRender, tags);
            }
            
            // Dessine la ROI            
            Rect ROI_plateau = plateau::calculateROI(Analyser.getCamPosAvg(), imageRender.cols, imageRender.rows);
            vector<Point2f> testVect;
            testVect.push_back(ROI_plateau.tl());
            testVect.push_back(ROI_plateau.tl()+Point2i(ROI_plateau.width, 0));
            testVect.push_back(ROI_plateau.tl()+Point2i(ROI_plateau.width, ROI_plateau.height));
            testVect.push_back(ROI_plateau.tl()+Point2i(0, ROI_plateau.height));
            plateau::draw::quad(imageRender, testVect, Scalar(255,110,160)); 
            
            // Dessine les tags
            for(auto tag : tags){                
                plateau::draw::tag(imageRender, tag);                
            }  

            if (c == '\r') {
                String pathToWrite = "test_panneau";
                pathToWrite.append(to_string(chronometer.getFrameNum()));
                pathToWrite.append(".png");
                imwrite(pathToWrite, imageRender);
            }         
            

            // Dessiner les candidats
            /*                            
            for(auto tag : bad_tags){                
                plateau::draw::quad(image, tag.corners, Scalar(20,255,20));                
            }
            */

            // Redimensionne et affiche l'image            
            resize(imageRender, sortie, Size(1440,1080));                            
            imshow("Image", sortie);
            
            // Compteur de FPS
            chronometer.stopChrono();
            chronometer.displayChrono();

            // Increment frame index
            frameIndex++;
            printf("~~~~~~~~~~~~~~~~~~~~~     END OF FRAME     ~~~~~~~~~~~~~~~~~~~~~\n");
        }
        c = waitKey(1);
    }
    cam->stopVideo();    
    destroyAllWindows();
    return 0;
}

