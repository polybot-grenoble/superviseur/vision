#ifndef __VISION__CONSTANTES_CONSTANTES__
#define __VISION__CONSTANTES_CONSTANTES__

#include <opencv2/core.hpp>

namespace CONSTANTES{
    // Image
    inline constexpr double image_scale = 0.80;     // units?

    // Position des tags sur le plateau
    inline constexpr double tag_20_x = 750.0;
    inline constexpr double tag_20_y = 1500.0;
    inline constexpr double tag_21_x = 2250.0;
    inline constexpr double tag_21_y = 1500.0;
    inline constexpr double tag_22_x = 750.0;
    inline constexpr double tag_22_y = 500.0;
    inline constexpr double tag_23_x = 2250.0;
    inline constexpr double tag_23_y = 500.0;

    // Taille du plateau
    inline constexpr double plateau_hauteur = 2000.0;
    inline constexpr double plateau_largeur = 3000.0;
    inline constexpr double plateau_centre_x = 1500.0;
    inline constexpr double plateau_centre_y = 1000.0;

    // Robots
    inline constexpr double robot_hauteur = 430.0;
    inline constexpr int robot_tag_range_BLEUE[2] = {1,5};
    inline constexpr int robot_tag_range_JAUNE[2] = {6,10};

    // Plants
    inline constexpr double plant_zone_scale = 1.2;
    inline constexpr double plant_heigth = 80.0;
    inline constexpr int plant_zone_number = 6;
    inline constexpr int plant_per_zone = 6;
    
    // Jardinières
    inline constexpr double jardi_width = 150.0;
    inline constexpr double jardi_length = 325.0;
    inline constexpr double jardi_heigth = 70.0;
    inline constexpr int jardi_zone_number = 6;

    // Paramètres d'analyse
    inline constexpr double analyse_tag_perimeter_min = 100.0;
    inline constexpr double analyse_tag_perimeter_max = 500.0;
    inline constexpr double analyse_deviation_safe_value = 100.0;
    inline constexpr double analyse_equipe_detection_margin = 30.0;

    // IDs
    inline constexpr int id_zone_robots = 100;
    inline constexpr int id_zone_plantes = 200;
    inline constexpr int id_zone_jardis = 300;
    
}



#endif